
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Parameters.h"

Parameters *Parameters::instance = NULL;

Parameters::Parameters() {
    runsNb = 1;
    yearsNb = 9;
    weeksNbPerYear = 52;
    daysNbPerYear = 365;
    dontInfect = false;
    vDataFiles = {"BirthFemaleEvents_withId.csv", "CullingRates_withId.csv", "Headcounts_withId.csv", "Network_withId_and_breedType_coderace.csv"};
    dataPath = "../data/";
    resultsPath = "../results/";
    initCondFile = "selectedInitScenarioForPrev_1to90.csv";
    herdsToRewireFile = "herdsToRewire.txt";
    selectedHerdsFile = "";
    initOutFile = ""; //To record the initial 'state' of each infected herd: 1 line = 1 infected herd with id, nbInfectedAnimals, prevalence and repetitionNum referring to a line in 'initCondFileName'
    initInFile = ""; //To set the initial 'state' of each infected herd: 1 line = 1 infected herd with id, nbInfectedAnimals, prevalence and repetitionNum referring to a line in 'initCondFileName'
    initPrevFile = ""; //To set the initial prevalence (among all animals, not only adults) of each herd: 1 line = 1 integer, which is the prevalence for this herd (herds are in the same order as in input data files)
    newMovesOutFile = "newMoves_run";
    isRewiringAllowed = true; //True by default to handle rewiring only with "options" (see main.cpp), but if useHerdHistory is on, then desactivate all rewiring options for the first 2 years
    doRewiring = false;
    doRewiringForOutside = false;
    doRewiringForbiddenMoves = false;
    doRewiringForBuyers = false;
    doRewiringForSellers = false;
    useSelectedHerdsToRewire = false;
    propOfHerdsToRewire = 0.;
    defaultCalfExpo = 1.0; // Calves exposure to the global environment
    useCalfExpoForSelectedHerds = false;
    calfExpoForSelectedHerds = 1.0;
    useCalfExpoForSpecHerds = false;
    specHerdForCalfExpo = eA;
    calfExpoForSpecHerds = 1.0;
    useSelectedHerds = false;
    doSavingOfInitialInfection = false;
    doLoadingOfInitialInfection = false;
    doLoadingOfInitialPrev = false;
    useBreed = false;
    breedToRewire = 66;
    useHerdHistory = false;
    herdStatusNb = 3;
    useAnnualPrev = false;
    useSameSensitivity = false;
    usePurchaseProba = false;
    probaToPurchaseS = 1.;
    areDetectedIhCulledEarlier = false;
    cullingProbaOfDetectedIh = 1 - std::exp(-(1/26.));
    propOfHerdsToInfect = 0.1;
    maxInitPrev = 90;
    vShedPropThres = {0.07, 0.21, 1.0}; //thresholds of infected animals proportion to classify herds
    vHerdPropPerStatus = {0.4, 0.4, 0.2}; //so initialize 40% of herds with prevalence < 7%, etc...
    vWeeksNbPerDeathRate = {2, 7, 121}; //e.g. if vector = {2, 7, 121}:
    //animals aged (in weeks) from 0 to 1 will die at a rate that is second in data file (after death rate at birth)
    //animals aged from 2 to 8 will die at a rate that is third in data file
    //animals aged from 9 to 129 will die at a rate that is fourth in data file
    categoriesNbForCows = 0; //e.g. if 5, there will be 5 rates for cows: cows of 1 year and 130 weeks, 2 years and 130 weeks,..., 5 and more years and 130 weeks => will be changed from data! If 0, error...
    outsideMetapop = "outside";
    minWeekToUpdateHerdState = 1; //First week of January
    maxWeekToUpdateHerdState = 17; //Last week of April

    _weekNumber = 0;
    _currentTime = 0;

    stderrGaussian = 0.347; //0.15 => ~sigma (can't be <0)
    meanGaussian = -0.42; //0 => ~mu

    // DEMOGRAPHIC PARAMETERS
    // ==========================================================
    _ageWeaning = 10;				// there is no more feeding with milk/colostrum
    _ageGrazing = 26;				// animal can go outside during pasture time
    _ageYoungHeifer = 52;			// animal become young heifer (from weaned out calf)
    _ageHeifer = 91;				// animal become heifer (from young heifer) => reproduction or IA at 91
    _ageFirstCalving = 130; 		// first calving => animal become cow (adult)
    _nbStagesOfLactation = 5;		// Number of lactating stages

    _durSusceptible = 52; 	// Animals are susceptible only the first year
    _durShedding = 25;		// mean time of It period
    _durIl = 52;			// mean time of Il period
    _durIm = 2*52;			// mean time of Im period

    _deathRateIm = 0.;              // Death-rate for Im animals
    _deathRateIh = 0.;              // Death-rate for Ih animals
    _cullingRateIh = 1./26.;        // Culling rate for Ih animals
    _cullingRateIm = 0.;            // Culling rate for Im animals
    _cullingProbaIh = 1 - std::exp(-(_cullingRateIh));        // Culling rate for Ih animals, transformed in proba
    _deathRateBactInside = 0.4;     // Elimination of bacteria in building
    _deathRateBactOutside = 1./14.; // Elimination of bacteria in pasture
    _cleaningCP = 1./6.;            // cleaning of collective pens
    // Adults contribute together to the environment
    _adultsTogether = 1.;

    // TRANSMISSION PARAMETERS
    // ==========================================================
    _colostrumTR = true;        // colostrum route
    _milkTR = true;             // milk route
    _localTR = true;            // local environment route
    _susceptibilityCoeff = 0.1; // Susceptibility of animal follows an exponential decrease : exp( -h*(age-1) )
    // Map infectious dose (quantity of bact needed for an infection)
    _infectiousDose = std::pow(10, 6);
    // Infection rate
    _infGeneralEnv = 9.5 * (std::pow(10, -6.802)) * 7;
    _infLocalEnv = 5 * (std::pow(10, -5)) * 7;
    _infGrazing = 5 * (std::pow(10, -6)) * 7;
    _infIngestion = 5 * (std::pow(10, -4)) * 7;

    // Vertical transmission (in utero or immediately after birth by contact between dam and daughter)
    _infinuteroIt = 0.149;
    _infinuteroIl = 0.149;
    _infinuteroIm = 0.149;
    _infinuteroIh = 0.65;

    // BACTERIA ENVIRONMENTS PARAMETERS
    // ==========================================================
    // Quantity of faeces produced in kg per week
    _qtyFaecesCnw = 0.5*7.;     // Unweaned calf (< 200kg)
    _qtyFaecesCw = 5.5*7.;		// Weaned calf (~ 200kg)
    _qtyFaecesHf = 10*7.;		// Heifer (~ 300kg)
    _qtyFaecesAd = 30*7.;		// Cow (~ 600kg)
    // Quantity of milk and colostrum drunk and produced
    _qtyMilkDrunk = 7.*7.;
    _qtyColoDrunk = 5.*3.;
    _qtyMilkProd = 25.*7.;
    _qtyColoProd = 25.*7.;
    // Proportion of lactating cow
    _propLactatingCows = 0.85;
    // Milk production by infected cow (reduced by infection)
    _qtyMilkProdIl = _qtyMilkProd * (1.-0.08);
    _qtyMilkProdIm = _qtyMilkProd * (1.-0.11);
    _qtyMilkProdIh = _qtyMilkProd * (1.-0.25);
    // Map shedding in faeces
    // - for calves
    _alphaFaecesIt = 8.8;
    _betaFaecesIt = 19.;
    // - for Adults
    _alphaFaecesIm = 2.65;
    _betaFaecesIm = 17;
    _alphaFaecesIh = 2;
    _betaFaecesIh = 17;

    // Map shedding in milk
    // - direct shedding
    _alphaMilkDirIm = 8.;
    _betaMilkDirIm = 8.;
    _alphaMilkDirIh = 8.;
    _betaMilkDirIh = 8.;
    // - indirect shedding (faecal contamination)
    _alphaMilkIndirIm = 1.;
    _betaMilkIndirIm = 25.;
    _alphaMilkIndirIh = 50.;
    _betaMilkIndirIh = 200.; //30
    // Map shedding in colostrum
    // - direct shedding
    _alphaColoDirIm = _alphaMilkDirIm;
    _betaColoDirIm = _betaMilkDirIm;
    _alphaColoDirIh = _alphaMilkDirIh;
    _betaColoDirIh = _betaMilkDirIh;
    // - indirect shedding(faecal contamination)
    _alphaColoIndirIm = _alphaMilkIndirIm;
    _betaColoIndirIm = _betaMilkIndirIm;
    _alphaColoIndirIh = _alphaMilkIndirIh;
    _betaColoIndirIh = _betaMilkIndirIh;
    // Map shedding in milk and colostrum for Im and Ih
    _propExcreMilkIm = 0.40;
    _propExcreMilkIh = 0.90;
    _propExcreColoIm = 0.40;
    _propExcreColoIh = 0.90;

    // TEST PARAMETERS
    // ==========================================================
    _testSensitivityIt = 0.15;			// Characteristics of the test : see Nielsen et Toft 2008 and More et al 2015
    _testSensitivityIl = 0.15;
    _testSensitivityIm = 0.47;
    _testSensitivityIh = 0.71;
    _testSpecificity = 1.0;
}

Parameters& Parameters::getInstance(){
    if (instance == NULL)
        instance = new Parameters();
    return *instance;
}

void Parameters::kill(){
    if (instance != NULL)
        delete instance;
    instance = NULL;
}

void Parameters::printAll(){
    std::cout << "runsNb                        " << yearsNb                << std::endl;
    std::cout << "yearsNb                       " << yearsNb                << std::endl;
    std::cout << "input data files:             "; for (auto s : vDataFiles) {std::cout << s << " ";} std::cout << std::endl;
    std::cout << "dataPath                      " << dataPath               << std::endl;
    std::cout << "resultsPath                   " << resultsPath            << std::endl;
    std::cout << "initCondFile                  " << initCondFile           << std::endl;
    std::cout << "doRewiring                    " << doRewiring             << std::endl;
    std::cout << "all shedders prop thresholds: "; for (auto th : vShedPropThres) {std::cout << th << " ";} std::cout << std::endl;
    std::cout << "all age thresholds for moves: "; for (auto th : vAgeThresForMoves) {std::cout << th << " ";} std::cout << std::endl;
    std::cout << "all age thresholds:           "; for (auto th : vAgeThres) {std::cout << th << " ";} std::cout << std::endl;
}

void Parameters::updateAfterSetting(){
    _totalTimeSteps = yearsNb * weeksNbPerYear +1 +1; //+2 weeks, because initialization = 0 and 1 extra week in movements data (469 dates, but 9 years * 52 sem = 468), 1 extra week also in births data (469 steps)
    vAgeThres = {1, _ageWeaning, _ageGrazing, _ageYoungHeifer, _ageHeifer, _ageFirstCalving};
    vAgeThresForMoves = {10, 26, 52, 91, 101, 111, 121, _ageFirstCalving};
}

void Parameters::initTime(){
    _currentTime = 0;
    _weekNumber = 0;
}

void Parameters::updateWeekNum(){
    if (_weekNumber < 51){
        _weekNumber += 1;
    } else {
        _weekNumber = 0;
    }
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <vector>
#include <map>

#include "Parameters.h"
#include "herd/Animal.h"

class Metapop {
protected:
    static Metapop* s_instance;
    Metapop();

    int ageGroupsNb;
    int healthStatesNb;
    std::vector<std::vector<int>> _vvNbPerGrpPerState;
    std::vector<std::vector<double>> _vvProbaPerGrpPerState;
    std::map<float, std::vector<std::vector<int>>> _mvvPrevThresToNbPerGrpPerState;
    std::map<float, std::vector<std::vector<double>>> _mvvPrevThresToProbaPerGrpPerState;

public:
    static Metapop& getInstance();
    static void kill();

    std::vector<double>& getHealthStateProbas(int ageGroup);
    std::vector<double>& getHealthStateProbas(int ageGroup, int status);
    void reset();
    void update(int status, std::vector<std::vector<int>>& vvNbPerGrpPerState);
    void computeHealthStateProbaPerAgeGroup();
};

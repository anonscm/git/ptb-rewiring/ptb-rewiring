
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Animal.h"
#include "Herd.h"
#include "AgeGroup/AnimalAgeGroup.h"

Animal::Animal(Herd* pHerd, int ag, short int timeInAgegroup, int hs, int dam_hs) { //used for calvings
	Parameters& param = Parameters::getInstance();
    _pHerd = pHerd;
    _shptrAgeGroup = _pHerd->getAnimalAgeGroup(ag);
	switch (ag) {
		case enumAnimal::newBorn :
			_age = 0;
			if (hs > enumAnimal::It) _healthStatus = enumAnimal::It;
			else _healthStatus = hs;
			break;
		case enumAnimal::unweanedCalf :
			if (timeInAgegroup >= 0 and timeInAgegroup < param._ageWeaning - 1) {
				_age = 1 + timeInAgegroup;
			} else {
				_age = 1 + Utils::uniform(param._ageWeaning);
			}
			if (hs > enumAnimal::Il) _healthStatus = enumAnimal::Il;
			else _healthStatus = hs;
			break;
		case enumAnimal::weanedInCalf :
			if (timeInAgegroup >= 0 and timeInAgegroup < (param._ageGrazing - param._ageWeaning)) {
				_age = param._ageWeaning + timeInAgegroup;
			} else {
				_age = param._ageWeaning + Utils::uniform(param._ageGrazing - param._ageWeaning);
			}
			if (hs > enumAnimal::Il) _healthStatus = enumAnimal::Il;
			else _healthStatus = hs;
			break;
		case enumAnimal::weanedOutCalf :
			if (timeInAgegroup >= 0 and timeInAgegroup < (param._ageYoungHeifer - param._ageGrazing)) {
				_age = param._ageGrazing + timeInAgegroup;
			} else {
				_age = param._ageGrazing + Utils::uniform(param._ageYoungHeifer - param._ageGrazing);
			}
			if (hs > enumAnimal::Il) _healthStatus = enumAnimal::Il;
			else _healthStatus = hs;
			break;
		case enumAnimal::youngHeifer :
			if (timeInAgegroup >= 0 and timeInAgegroup < (param._ageHeifer - param._ageYoungHeifer)) {
				_age = param._ageYoungHeifer + timeInAgegroup;
			} else {
				_age = param._ageYoungHeifer + Utils::uniform(param._ageHeifer - param._ageYoungHeifer);
			}
			if (hs > enumAnimal::Il) _healthStatus = enumAnimal::Il;
			else _healthStatus = hs;
			break;
		case enumAnimal::heifer :
			if (timeInAgegroup >= 0 and timeInAgegroup < (param._ageFirstCalving - param._ageHeifer)) {
				_age = param._ageHeifer + timeInAgegroup;
			} else {
				_age = param._ageHeifer + Utils::uniform(param._ageFirstCalving - param._ageHeifer);
			}
			_healthStatus = hs;
			break;
		case enumAnimal::cow :
			if (timeInAgegroup >= 1 and timeInAgegroup <= 5) {
				_parity = timeInAgegroup;
			} else {
				_parity = 1;
			}
			_age = param._ageFirstCalving + (_parity-1) * int(_pHerd->getDLacta()) + Utils::uniform(int(_pHerd->getDLacta()));
			if (hs == enumAnimal::It) _healthStatus = enumAnimal::Il;
			else _healthStatus = hs;
			break;
	}
	_damHealthStatus = dam_hs;
    _cullingProbaIh = param._cullingProbaIh;
}

Animal::Animal(Herd* pHerd, int age, int hs, int parity) { //used to initialize herds, to introduce (infected) animal, to send animals from outside, or for calving when no dam available
    Parameters& iParams = Parameters::getInstance();
    _age = age;
    if (age == 0){
        _ageGroup = enumAnimal::newBorn;
        if (hs > enumAnimal::It) _healthStatus = enumAnimal::It;
        else _healthStatus = hs;
    } else if (age >= iParams._ageHeifer and age < iParams._ageFirstCalving) {
        _ageGroup = enumAnimal::heifer;
        _healthStatus = hs;
    } else if (age >= iParams._ageFirstCalving) {
        _parity = parity;
        _ageGroup = enumAnimal::cow;
        if (hs == enumAnimal::It) _healthStatus = enumAnimal::Il;
        else _healthStatus = hs;
    } else {
        for (auto i = 1; i < iParams.vAgeThres.size(); ++i){
            if (age < iParams.vAgeThres[i]){
                _ageGroup = i;
                if (hs > enumAnimal::Il) _healthStatus = enumAnimal::Il;
                else _healthStatus = hs;
                break;
            }
        }
	}
    if (pHerd != nullptr){ //used for animals coming from "outside", so it doesn't know the herd, neither the age group (instance)
        _pHerd = pHerd;
        _shptrAgeGroup = _pHerd->getAnimalAgeGroup(_ageGroup);
    }
    _cullingProbaIh = iParams._cullingProbaIh;
}

Animal::~Animal(){
    _mptrOffspring.clear();
}

void Animal::initializeTimeBeforeNextCalving(){
    _timeBeforeNextCalving = _pHerd->getCalvingInterval();
}

void Animal::updateTimeBeforeNextCalving(){
    if (_timeBeforeNextCalving > 0){
        _timeBeforeNextCalving -= 1;
    }
}

RepetitionHerdState& Animal::getHerdState(){
    return _pHerd->getHerdState();
}

MapShedding& Animal::getMapShedding(){
    return _pHerd->getMapShedding();
}

MapInEnvironments& Animal::getMapInEnv(){
    return _pHerd->getMapInEnv();
}

int Animal::getGroupOfAge() {
    if (_shptrAgeGroup != nullptr){
    	return (*_shptrAgeGroup).getAgeGroup();
    } else { return _ageGroup; }
}

void Animal::setDamPtr(Animal* animal) {
	_ptrDam = animal;
}

Animal* Animal::getDamPtr() {
	return _ptrDam;
}

void Animal::deleteDamPtr() {
	_ptrDam = nullptr;
}

void Animal::addNewOffspring(Animal* animal) {
	_mptrOffspring[animal] = nullptr;
}

void Animal::deleteOneOffspring(Animal* animal) {
	_mptrOffspring.erase(animal);
}

void Animal::clearOffspringMap() {
	if (_mptrOffspring.size() > 0) {
		for (auto it = _mptrOffspring.begin(); it != _mptrOffspring.end(); it++) {
			(*it).first->deleteDamPtr();
		}
	}
	_mptrOffspring.erase(_mptrOffspring.begin(), _mptrOffspring.end());
}

void Animal::saveHeadcount (int t) {
	(*_shptrAgeGroup).saveHeadcount(t, _pHerd->getHerdState(), (*this));
}

int Animal::calving(int t) {
	return (*_shptrAgeGroup).calving(t, (*this));
}

void Animal::exitAging(int t) {
   	(*_shptrAgeGroup).exitAging(t, (*this));
}

void Animal::updateConsumedInfectedColostrum() {
	(*_shptrAgeGroup).updateConsumedInfectedColostrum(_consumedInfectedColostrum, _damHealthStatus);
}

void Animal::shedding() {
	(*_shptrAgeGroup).shedding(_pHerd->getMapShedding(), _healthStatus, _parity);
}

void Animal::infection(int t) {
	(*_shptrAgeGroup).infection(t, (*this));
}

void Animal::transition(int t) {
	(*_shptrAgeGroup).transition(t, (*this));
}

std::string Animal::toString() {
	std::ostringstream address;
	address << (void const *)this;
	return (*_shptrAgeGroup).getAgeGroupStr() + std::string(" ("+ address.str() +") : age "+std::to_string(_age)+" and health status " + std::to_string(_healthStatus));
}

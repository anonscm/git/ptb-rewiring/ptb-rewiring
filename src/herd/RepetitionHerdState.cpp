
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "RepetitionHerdState.h"

RepetitionHerdState::RepetitionHerdState() {
	Parameters& param = Parameters::getInstance();

	_vCalvesNewBorn.resize(param._totalTimeSteps,0);
	_vCalvesUnWeaned.resize(param._totalTimeSteps,0);
	_vCalvesWeanedIn.resize(param._totalTimeSteps,0);
	_vCalvesWeanedOut.resize(param._totalTimeSteps,0);
    _vYoungHeifers.resize(param._totalTimeSteps,0);
    _vHeifers.resize(param._totalTimeSteps,0);
    _vCows.resize(param._totalTimeSteps,0);
    _vTwoYearsOld.resize(param._totalTimeSteps,0);
    _vHeadcount.resize(param._totalTimeSteps,0);

    _vMilkprodTot.resize(param._totalTimeSteps,0);
    _vMilkprodTot_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTot_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTot_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTot_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTot_P5.resize(param._totalTimeSteps,0);
    _vMilkprodTotR.resize(param._totalTimeSteps,0);
    _vMilkprodTotR_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTotR_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTotR_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTotR_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTotR_P5.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTotIt_P5.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTotIl_P5.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTotIm_P5.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh_P1.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh_P2.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh_P3.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh_P4.resize(param._totalTimeSteps,0);
    _vMilkprodTotIh_P5.resize(param._totalTimeSteps,0);

    _vHeadcountBuilding.resize(param._totalTimeSteps,0);
    _vHeadcountPasture.resize(param._totalTimeSteps,0);

    _vBirthsOverTimeS.resize(param._totalTimeSteps,0);
    _vBirthsOverTimeIt.resize(param._totalTimeSteps,0);

    _vExitCow.resize(param._totalTimeSteps,0);
    _vExitCowIh.resize(param._totalTimeSteps,0);

    _vStot.resize(param._totalTimeSteps,0);
    _vRtot.resize(param._totalTimeSteps,0);
    _vIttot.resize(param._totalTimeSteps,0);
    _vIltot.resize(param._totalTimeSteps,0);
    _vImtot.resize(param._totalTimeSteps,0);
    _vIhtot.resize(param._totalTimeSteps,0);
    _vPositiveIhtot.resize(param._totalTimeSteps,0);

    _vAgeAtInfection.resize(param._durSusceptible,0);
    _vAgeAtInfection_inutero.resize(param._durSusceptible,0);
    _vAgeAtInfection_colostrum.resize(param._durSusceptible,0);
    _vAgeAtInfection_milk.resize(param._durSusceptible,0);
    _vAgeAtInfection_local.resize(param._durSusceptible,0);
    _vAgeAtInfection_general.resize(param._durSusceptible,0);

    _vIncidenceIt.resize(param._totalTimeSteps,0);
    _vTransmissionRoutes_inutero.resize(param._totalTimeSteps,0);
    _vTransmissionRoutes_colostrum.resize(param._totalTimeSteps,0);
    _vTransmissionRoutes_milk.resize(param._totalTimeSteps,0);
    _vTransmissionRoutes_local.resize(param._totalTimeSteps,0);
    _vTransmissionRoutes_general.resize(param._totalTimeSteps,0);

    _vIncidenceIl.resize(param._totalTimeSteps,0);
    _vIncidenceIm.resize(param._totalTimeSteps,0);
    _vIncidenceIh.resize(param._totalTimeSteps,0);

    _vIncidenceCumulatedIt.resize(param._totalTimeSteps,0);
    _vIncidenceCumulatedIl.resize(param._totalTimeSteps,0);
    _vIncidenceCumulatedIm.resize(param._totalTimeSteps,0);
    _vIncidenceCumulatedIh.resize(param._totalTimeSteps,0);

    _vInfected.resize(param._totalTimeSteps,0);
    _vInfectious.resize(param._totalTimeSteps,0);
    _vAffected.resize(param._totalTimeSteps,0);

    _vInfectedAdults.resize(param._totalTimeSteps,0);
    _vPositiveTwoYearsOld.resize(param._totalTimeSteps,0);
    _vInfectedTwoYearsOld.resize(param._totalTimeSteps,0);
    _vIlImIh.resize(param._totalTimeSteps,0);

    _vPersistence.resize(param._totalTimeSteps,0);
}

void RepetitionHerdState::update(int tnow) {
	_vHeadcount[tnow] += _vCalvesNewBorn[tnow] + _vCalvesUnWeaned[tnow] + _vCalvesWeanedIn[tnow] +
			_vCalvesWeanedOut[tnow] +	_vYoungHeifers[tnow] + _vHeifers[tnow] + _vCows[tnow];
	_vInfected[tnow] += _vIttot[tnow] + _vIltot[tnow] + _vImtot[tnow] + _vIhtot[tnow];
	_vInfectious[tnow] += _vIttot[tnow] + _vImtot[tnow] + _vIhtot[tnow];
	_vAffected[tnow] += _vIhtot[tnow];
	_vIlImIh[tnow] = _vIltot[tnow] + _vImtot[tnow] + _vIhtot[tnow];
}

void RepetitionHerdState::updatePersistence(int tnow) {
	_vPersistence[tnow] = 1;
}

void RepetitionHerdState::updateIncidenceCumulated() {
	Parameters& param = Parameters::getInstance();
	for (int t=1; t < param._totalTimeSteps; t++) {
	    _vIncidenceCumulatedIt[t] += _vIncidenceCumulatedIt[t-1] + _vIncidenceIt[t];
	    _vIncidenceCumulatedIl[t] += _vIncidenceCumulatedIl[t-1] + _vIncidenceIl[t];
	    _vIncidenceCumulatedIm[t] += _vIncidenceCumulatedIm[t-1] + _vIncidenceIm[t];
	    _vIncidenceCumulatedIh[t] += _vIncidenceCumulatedIh[t-1] + _vIncidenceIh[t];
	}
}

bool RepetitionHerdState::infectedRemaining(int tnow) {
	if (_vInfected[tnow] == 0) {
		return false;
	} else {
		return true;
	}
}

void RepetitionHerdState::printData(int t) {
	std::cout << "## Population information at time t=" << t << " ##" << std::endl;
	std::cout << "## New born calves   : " << _vCalvesNewBorn[t] << " ##" << std::endl;
	std::cout << "## Unweaned calves   : " << _vCalvesUnWeaned[t] << " ##" << std::endl;
	std::cout << "## Weaned in calves  : " << _vCalvesWeanedIn[t] << " ##" << std::endl;
	std::cout << "## Weaned out calves : " << _vCalvesWeanedOut[t] << " ##" << std::endl;
	std::cout << "## Young Heifers     : " << _vYoungHeifers[t] << " ##" << std::endl;
	std::cout << "## Heifers           : " << _vHeifers[t] << " ##" << std::endl;
	std::cout << "## Cows              : " << _vCows[t] << " ##" << std::endl << std::endl;
	std::cout << "## pop total         : " << _vHeadcount[t] << " ##" << std::endl << std::endl;
	std::cout << "## S tot             : " << _vStot[t] << " ##" << std::endl;
	std::cout << "## T tot             : " << _vIttot[t] << " ##" << std::endl;
	std::cout << "## L tot             : " << _vIltot[t] << " ##" << std::endl;
	std::cout << "## R tot             : " << _vRtot[t] << " ##" << std::endl;
	std::cout << "## Is tot            : " << _vImtot[t] << " ##" << std::endl;
	std::cout << "## Ic tot            : " << _vIhtot[t] << " ##" << std::endl;
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <vector>
#include <iterator>
#include <memory>
#include <iostream>
#include <string>
#include <iomanip>
#include <math.h>
#include <assert.h>

class Animal;

#include "../Parameters.h"
#include "../Metapop.h"
#include "../Utils.h"
#include "RepetitionHerdState.h"
#include "map/MapShedding.h"
#include "map/MapInEnvironments.h"
#include "../MovementUnit.h"

class Herd {
private:
    std::string _id;
    int _startGrazing;
    int _endGrazing;
    int _weekToUpdateState;
    float _infectedProp;
    int _status;
    double _dLacta;
    short int _calvingInterval;
    double _deathRateBirth;
    std::vector<double> _vDeathRate;
    std::vector<std::vector<float>> _vvCullingRatesPerYearPerGroup;
    std::vector<int> _vBirthsNbPerStep;
    double _unweanedExposure;
    double _weanedExposure;
    RepetitionHerdState _iHerdState;
    MapShedding _iMapShedding;
    MapInEnvironments _iMapInEnv;
    bool _grazing;
    std::vector<double> _vCullingRate;
    std::vector<std::vector<int>> _vvCurrentNbPerGrpPerState;

public:
    std::vector<std::shared_ptr<Animal>> _vAnimal;
    std::vector<std::shared_ptr<Animal>> _vAnimal_tmp;
    std::map<int, std::shared_ptr<AnimalAgeGroup>> _mshptr_AnimalAgeGroups;

    Herd(std::string id);
    ~Herd();

    double getDLacta();
    double getDLactaI();
    short int getCalvingInterval();
    double getDeathRateBirth();
    std::vector<double>& getDeathRates();
    std::vector<double>& getCullingRates();

    bool isGrazingPeriod();
    bool isSellingPeriod();
    bool isPersistent(int t);

    double getUnweanedExpo();
    double getWeanedExpo();
    void setUnweanedExpo(float expo);
    void setWeanedExpo(float expo);
    RepetitionHerdState& getHerdState();
    MapShedding& getMapShedding();
    MapInEnvironments& getMapInEnv();

    int getNumberOfAnimals(int ageGroup);
    int getNumberOfAnimalsInBuilding();
    int getNumberOfAnimalsInPasture();
    std::shared_ptr<AnimalAgeGroup> getAnimalAgeGroup(int ag);

    void resetNbPerGrpPerState();
    void updateRates(std::vector<float> vRatesPerGroup);
    void updateEachYear(int yearNum);
    void computeAnimalInfectedProp();
    virtual int getStatus(); //virtual for tests...
    virtual void setStatus(); //virtual for tests...
    float getAdultsPrevalence();
    // Functions to initialize herd
    std::tuple<int, int, int> computeAnimalNbToInfect(int herdSize, std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge);
    std::tuple<int, int> computeAnimalNbToInfect(int herdSize, std::vector<std::vector<std::vector<double>>>& vvvRepetOfNbPerStatePerAge);
    std::tuple<int, int> pickAgeAndStateOfInfectedAnimal(std::vector<std::vector<double>>& vvNbPerStatePerAge, std::vector<std::vector<double>>& vvMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge);
    void infectEnv(std::vector<std::vector<double>>& vvNbPerStatePerAge, std::vector<std::tuple<int, int>>& vtAgeAndStateOfInfectedAnimals);
    std::vector<std::tuple<int, int>> initializeInfection(std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge);
    std::vector<std::tuple<int, int>> initializeInfection(std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge, std::vector<int>& vInfectedNbAndPrevAndRepet);
    std::vector<std::tuple<int, int>> initializeInfection(int prev, std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge);
    void createAnAnimal(int age, int healthState, std::vector<std::vector<int>>& vvNbPerGrpPerState);
    void initializeAnimals(std::vector<int>& vHeadcount, std::vector<std::tuple<int, int>>& vtAgeAndStateOfInfectedAnimals);
    void initializeRates(std::vector<int>& vBirthsPerStep, std::vector<float>& vCullingRatesPerYearPerGroup);
    // Function to link randomly all animals under or in the minimum age group to a dam
    void linkOffspringsToDam(enumAnimal::AgeGroup minAgeGroup);

    void addPendingMoves(std::vector<MovementUnit>& vPendingMoves);
    void sendMoves(std::vector<MovementUnitFromData>& vMovesToSend, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesToOutside);

    void runDynamics(int t);
    void updateParams();
    void updateCalving(std::shared_ptr<Animal>& animal, int t);
    void updateSheddingCalvingExitingAging(int t);
    void updateTransitionInfection(int t);
    std::vector<int> getHeadcount();
    void deleteExitAnimals();
    void transferTmpAnimals();
    void clearHerd() {_vAnimal.clear();}
    void clearHerdTmp() {_vAnimal_tmp.clear();}
    void clearAgeGroup() {_mshptr_AnimalAgeGroups.clear();}

    int size() {return _vAnimal.size();}
};

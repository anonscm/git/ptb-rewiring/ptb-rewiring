
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Herd.h"
#include "Animal.h"
#include "AgeGroup/NewBorn.h"
#include "AgeGroup/UnweanedCalf.h"
#include "AgeGroup/WeanedInCalf.h"
#include "AgeGroup/WeanedOutCalf.h"
#include "AgeGroup/YoungHeifer.h"
#include "AgeGroup/Heifer.h"
#include "AgeGroup/Cow.h"

#include <fstream>

Herd::Herd(std::string id){
    Parameters& iParams = Parameters::getInstance();

    _mshptr_AnimalAgeGroups[enumAnimal::newBorn] = std::make_shared<NewBorn>();
    _mshptr_AnimalAgeGroups[enumAnimal::unweanedCalf] = std::make_shared<UnweanedCalf>();
    _mshptr_AnimalAgeGroups[enumAnimal::weanedInCalf] = std::make_shared<WeanedInCalf>();
    _mshptr_AnimalAgeGroups[enumAnimal::weanedOutCalf] = std::make_shared<WeanedOutCalf>();
    _mshptr_AnimalAgeGroups[enumAnimal::youngHeifer] = std::make_shared<YoungHeifer>();
    _mshptr_AnimalAgeGroups[enumAnimal::heifer] = std::make_shared<Heifer>();
    _mshptr_AnimalAgeGroups[enumAnimal::cow] = std::make_shared<Cow>();

    _id = id;
    _dLacta = 56.3;
    _calvingInterval = iParams.weeksNbPerYear + 1;
    _startGrazing = 14;		// week number of pasture beginning : start of April
    _endGrazing = 46;		// week number of pasture ending    : mid November
    _grazing = false;
    _weekToUpdateState = Utils::uniform(iParams.minWeekToUpdateHerdState, iParams.maxWeekToUpdateHerdState);
    _infectedProp = 0;
    _status = eA;
    _weanedExposure = iParams.defaultCalfExpo;
    _unweanedExposure = iParams.defaultCalfExpo;
    resetNbPerGrpPerState();
}

Herd::~Herd() {
    clearHerd();
    clearHerdTmp();
    clearAgeGroup();
}

double Herd::getDLacta(){
    return _dLacta;
}

short int Herd::getCalvingInterval(){
    return _calvingInterval;
}

double Herd::getDeathRateBirth(){
    return _deathRateBirth;
}

std::vector<double>& Herd::getDeathRates(){
    return _vDeathRate;
}

std::vector<double>& Herd::getCullingRates(){
    return _vCullingRate;
}

bool Herd::isGrazingPeriod(){
    return _grazing;
}

double Herd::getUnweanedExpo(){
    return _unweanedExposure;
}

double Herd::getWeanedExpo(){
    return _weanedExposure;
}

void Herd::setUnweanedExpo(float expo){
    _unweanedExposure = expo;
}

void Herd::setWeanedExpo(float expo){
    _weanedExposure = expo;
}

RepetitionHerdState& Herd::getHerdState(){
    return _iHerdState;
}

MapShedding& Herd::getMapShedding(){
    return _iMapShedding;
}

MapInEnvironments& Herd::getMapInEnv(){
    return _iMapInEnv;
}

int Herd::getNumberOfAnimals(int ageGroup){
    return _mshptr_AnimalAgeGroups[ageGroup].use_count()-1;
}

int Herd::getNumberOfAnimalsInBuilding(){
    if (_grazing){
        return (_mshptr_AnimalAgeGroups[enumAnimal::newBorn].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::unweanedCalf].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::weanedInCalf].use_count()-1);
    } else {
        return (_mshptr_AnimalAgeGroups[enumAnimal::newBorn].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::unweanedCalf].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::weanedInCalf].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::weanedOutCalf].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::youngHeifer].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::heifer].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::cow].use_count()-1);
    }
}

int Herd::getNumberOfAnimalsInPasture(){
    if (_grazing){
        return (_mshptr_AnimalAgeGroups[enumAnimal::weanedOutCalf].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::youngHeifer].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::heifer].use_count()-1)
             + (_mshptr_AnimalAgeGroups[enumAnimal::cow].use_count()-1);
    } else {
        return 0;
    }
}

std::shared_ptr<AnimalAgeGroup> Herd::getAnimalAgeGroup(int ag){
    return _mshptr_AnimalAgeGroups.find(ag)->second;
}

std::tuple<int, int, int> Herd::computeAnimalNbToInfect(int herdSize, std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge){
    Parameters& iParams = Parameters::getInstance();
    double truePrevalence = Utils::gaussian(iParams.meanGaussian, iParams.stderrGaussian);
    int prev = std::round(truePrevalence * 100);
    while ((truePrevalence < 0) or (prev > iParams.maxInitPrev)) {
        truePrevalence = Utils::gaussian(iParams.meanGaussian, iParams.stderrGaussian);
        prev = std::round(truePrevalence * 100);
    }
    if (prev <= 0){
        return std::make_tuple(0, 0, 0);
    } else {
        int selectedRepet = Utils::uniform(mvvvPrevToRepetOfNbPerStatePerAge[prev].size());
        double probaOfSR = mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet][0][0];
        return std::make_tuple(std::round(herdSize * (1. - probaOfSR)), prev, selectedRepet);
    }
}

std::tuple<int, int> Herd::computeAnimalNbToInfect(int herdSize, std::vector<std::vector<std::vector<double>>>& vvvRepetOfNbPerStatePerAge){
    int selectedRepet = Utils::uniform(vvvRepetOfNbPerStatePerAge.size());
    double probaOfSR = vvvRepetOfNbPerStatePerAge[selectedRepet][0][0];
    return std::make_tuple(std::round(herdSize * (1. - probaOfSR)), selectedRepet);
}

std::tuple<int, int> Herd::pickAgeAndStateOfInfectedAnimal(std::vector<std::vector<double>>& vvNbPerStatePerAge, std::vector<std::vector<double>>& vvMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge){
    Parameters& iParams = Parameters::getInstance();
    int ageSign = 92;
    int stagesNb = vHeadcountPerAge.size();
    std::vector<double> vProbaT_Distribution(vvNbPerStatePerAge[2].begin(), vvNbPerStatePerAge[2].end());
    std::vector<double> vProbaL_Distribution(vvNbPerStatePerAge[3].begin(), vvNbPerStatePerAge[3].end());
    std::vector<double> vProbaIs_Distribution(vvNbPerStatePerAge[4].begin(), vvNbPerStatePerAge[4].end());
    std::vector<double> vProbaIc_Distribution(vvNbPerStatePerAge[5].begin(), vvNbPerStatePerAge[5].end());
    std::vector<double> vIsThereAnimalPerAge = Utils::transformItemsByIsNotZero(vHeadcountPerAge);
    Utils::multiplyInFirstVector(vProbaT_Distribution, vIsThereAnimalPerAge);
    Utils::multiplyInFirstVector(vProbaL_Distribution, vIsThereAnimalPerAge);
    Utils::multiplyInFirstVector(vProbaIs_Distribution, vIsThereAnimalPerAge);
    Utils::multiplyInFirstVector(vProbaIc_Distribution, vIsThereAnimalPerAge);
    std::vector<double> vProbaAll;
    vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaT_Distribution), std::end(vProbaT_Distribution));
    vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaL_Distribution), std::end(vProbaL_Distribution));
    vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIs_Distribution), std::end(vProbaIs_Distribution));
    vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIc_Distribution), std::end(vProbaIc_Distribution));
    if (std::accumulate(vProbaAll.begin(), vProbaAll.end(), 0.0) == 0) {
        vProbaT_Distribution = vvMeanNbPerStatePerAge[2];
        vProbaL_Distribution = vvMeanNbPerStatePerAge[3];
        vProbaIs_Distribution = vvMeanNbPerStatePerAge[4];
        vProbaIc_Distribution = vvMeanNbPerStatePerAge[5];
        std::vector<double> vMinProbaCorrection_T1(iParams._durSusceptible, std::pow(10, -9));
        std::vector<double> vMinProbaCorrection_T2(stagesNb - iParams._durSusceptible, 0);
        std::vector<double> vMinProbaCorrection_T;
        vMinProbaCorrection_T.insert(std::end(vMinProbaCorrection_T), std::begin(vMinProbaCorrection_T1), std::end(vMinProbaCorrection_T1));
        vMinProbaCorrection_T.insert(std::end(vMinProbaCorrection_T), std::begin(vMinProbaCorrection_T2), std::end(vMinProbaCorrection_T2));
        Utils::accumulateInFirstVector(vProbaT_Distribution, vMinProbaCorrection_T);
        std::vector<double> vMinProbaCorrection_L(stagesNb, std::pow(10, -9));
        Utils::accumulateInFirstVector(vProbaL_Distribution, vMinProbaCorrection_L);
        std::vector<double> vMinProbaCorrection_IsIc1(ageSign, 0);
        std::vector<double> vMinProbaCorrection_IsIc2(stagesNb - ageSign, std::pow(10, -9));
        std::vector<double> vMinProbaCorrection_IsIc;
        vMinProbaCorrection_IsIc.insert(std::end(vMinProbaCorrection_IsIc), std::begin(vMinProbaCorrection_IsIc1), std::end(vMinProbaCorrection_IsIc1));
        vMinProbaCorrection_IsIc.insert(std::end(vMinProbaCorrection_IsIc), std::begin(vMinProbaCorrection_IsIc2), std::end(vMinProbaCorrection_IsIc2));
        Utils::accumulateInFirstVector(vProbaIs_Distribution, vMinProbaCorrection_IsIc);
        Utils::accumulateInFirstVector(vProbaIc_Distribution, vMinProbaCorrection_IsIc);
        Utils::multiplyInFirstVector(vProbaT_Distribution, vIsThereAnimalPerAge);
        Utils::multiplyInFirstVector(vProbaL_Distribution, vIsThereAnimalPerAge);
        Utils::multiplyInFirstVector(vProbaIs_Distribution, vIsThereAnimalPerAge);
        Utils::multiplyInFirstVector(vProbaIc_Distribution, vIsThereAnimalPerAge);
        vProbaAll.clear();
        vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaT_Distribution), std::end(vProbaT_Distribution));
        vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaL_Distribution), std::end(vProbaL_Distribution));
        vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIs_Distribution), std::end(vProbaIs_Distribution));
        vProbaAll.insert(std::end(vProbaAll), std::begin(vProbaIc_Distribution), std::end(vProbaIc_Distribution));
    }
    std::vector<int> vWhereValueNotZeroIndicateAgeAndState = Utils::multinomial(1, vProbaAll);
    assert(std::accumulate(vWhereValueNotZeroIndicateAgeAndState.begin(), vWhereValueNotZeroIndicateAgeAndState.end(), 0) == 1);
    auto it = std::find(std::begin(vWhereValueNotZeroIndicateAgeAndState), std::end(vWhereValueNotZeroIndicateAgeAndState), 1);
    int position = std::distance(vWhereValueNotZeroIndicateAgeAndState.begin(), it);
    int healthState = position / stagesNb;
    int age = position % stagesNb;
    vHeadcountPerAge[age] -= 1;
    return std::make_tuple(age, healthState);
}

void Herd::infectEnv(std::vector<std::vector<double>>& vvNbPerStatePerAge, std::vector<std::tuple<int, int>>& vtAgeAndStateOfInfectedAnimals){
    Parameters& iParams = Parameters::getInstance();
    double nb_calvesUnweaned = 0;
    double nb_calvesWeanedInOut = 0;
    double nb_yHeifer = 0;
    double nb_heifer = 0;
    double nb_lactAll = 0;
    for (auto tAgeAndState : vtAgeAndStateOfInfectedAnimals){
        int age = std::get<0>(tAgeAndState);
        if (age < iParams._ageWeaning){
            nb_calvesUnweaned += 1;
        } else if (age < iParams._ageYoungHeifer){
            nb_calvesWeanedInOut += 1;
        } else if (age < iParams._ageHeifer){
            nb_yHeifer += 1;
        } else if (age < iParams._ageFirstCalving){
            nb_heifer += 1;
        } else {
            nb_lactAll += 1;
        }
    }
    std::vector<double> vBactQttyInEnv(vvNbPerStatePerAge[6].begin(), vvNbPerStatePerAge[6].end());
    _iMapInEnv._vEnvInside_1[0] = vBactQttyInEnv[0] * nb_calvesUnweaned;
    _iMapInEnv._vEnvInside_2[0] = vBactQttyInEnv[1] * nb_calvesWeanedInOut;
    _iMapInEnv._vEnvInside_3[0] = vBactQttyInEnv[2] * nb_yHeifer;
    _iMapInEnv._vEnvInside_4[0] = vBactQttyInEnv[3] * nb_heifer;
    _iMapInEnv._vEnvInside_5[0] = vBactQttyInEnv[4] * nb_lactAll;
    _iMapInEnv._vEnvGeneral[0] = _iMapInEnv._vEnvInside_1[0] + _iMapInEnv._vEnvInside_2[0] + _iMapInEnv._vEnvInside_3[0] + _iMapInEnv._vEnvInside_4[0] + _iMapInEnv._vEnvInside_5[0];
}

std::vector<std::tuple<int, int>> Herd::initializeInfection(std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge){
    Parameters& iParams = Parameters::getInstance();
    std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
    int nbInfectedAnimals;
    int prev;
    int selectedRepet;
    int herdSize = std::accumulate(vHeadcountPerAge.begin(), vHeadcountPerAge.end(), 0);
    std::tie(nbInfectedAnimals, prev, selectedRepet) = computeAnimalNbToInfect(herdSize, mvvvPrevToRepetOfNbPerStatePerAge);
    if (iParams.doSavingOfInitialInfection){
        std::fstream f(iParams.initOutFile, std::fstream::app);
        f << _id << "\t" << nbInfectedAnimals << "\t" << prev << "\t" << selectedRepet << "\n";
        f.close();
    }
    for (auto i = 0; i < nbInfectedAnimals; i++) {
        vtAgeAndStateOfInfectedAnimals.emplace_back(pickAgeAndStateOfInfectedAnimal(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], mvvPrevToMeanNbPerStatePerAge[prev], vHeadcountPerAge));
    }
    if (nbInfectedAnimals > 0){
        infectEnv(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], vtAgeAndStateOfInfectedAnimals);
        _iHerdState.updatePersistence(0);
    }
    return vtAgeAndStateOfInfectedAnimals;
}

std::vector<std::tuple<int, int>> Herd::initializeInfection(std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge, std::vector<int>& vInfectedNbAndPrevAndRepet){
    Parameters& iParams = Parameters::getInstance();
    std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
    int nbInfectedAnimals = vInfectedNbAndPrevAndRepet[0];
    int prev = vInfectedNbAndPrevAndRepet[1];
    int selectedRepet = vInfectedNbAndPrevAndRepet[2];
    if (iParams.doSavingOfInitialInfection){
        std::fstream f(iParams.initOutFile, std::fstream::app);
        f << _id << "\t" << nbInfectedAnimals << "\t" << prev << "\t" << selectedRepet << "\n";
        f.close();
    }
    for (auto i = 0; i < nbInfectedAnimals; i++) {
        vtAgeAndStateOfInfectedAnimals.emplace_back(pickAgeAndStateOfInfectedAnimal(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], mvvPrevToMeanNbPerStatePerAge[prev], vHeadcountPerAge));
    }
    if (nbInfectedAnimals > 0){
        infectEnv(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], vtAgeAndStateOfInfectedAnimals);
        _iHerdState.updatePersistence(0);
    }
    return vtAgeAndStateOfInfectedAnimals;
}

std::vector<std::tuple<int, int>> Herd::initializeInfection(int prev, std::map<int, std::vector<std::vector<std::vector<double>>>>& mvvvPrevToRepetOfNbPerStatePerAge, std::map<int, std::vector<std::vector<double>>>& mvvPrevToMeanNbPerStatePerAge, std::vector<int>& vHeadcountPerAge){
    Parameters& iParams = Parameters::getInstance();
    std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
    int nbInfectedAnimals;
    int selectedRepet;
    int herdSize = std::accumulate(vHeadcountPerAge.begin(), vHeadcountPerAge.end(), 0);
    std::tie(nbInfectedAnimals, selectedRepet) = computeAnimalNbToInfect(herdSize, mvvvPrevToRepetOfNbPerStatePerAge[prev]);
    if (iParams.doSavingOfInitialInfection){
        std::fstream f(iParams.initOutFile, std::fstream::app);
        f << _id << "\t" << nbInfectedAnimals << "\t" << prev << "\t" << selectedRepet << "\n";
        f.close();
    }
    for (auto i = 0; i < nbInfectedAnimals; i++) {
        vtAgeAndStateOfInfectedAnimals.emplace_back(pickAgeAndStateOfInfectedAnimal(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], mvvPrevToMeanNbPerStatePerAge[prev], vHeadcountPerAge));
    }
    if (nbInfectedAnimals > 0){
        infectEnv(mvvvPrevToRepetOfNbPerStatePerAge[prev][selectedRepet], vtAgeAndStateOfInfectedAnimals);
        _iHerdState.updatePersistence(0);
    }
    return vtAgeAndStateOfInfectedAnimals;
}

void Herd::createAnAnimal(int age, int healthState, std::vector<std::vector<int>>& vvNbPerGrpPerState){
    Parameters& iParams = Parameters::getInstance();
    int parity = 0;
    if (age >= iParams._ageFirstCalving){
        parity = age - iParams._ageFirstCalving +1;
        age = (age - iParams._ageFirstCalving) * iParams.weeksNbPerYear + iParams._ageFirstCalving;
    }
    auto pAnimal = std::make_shared<Animal>(this, age, healthState, parity);
    pAnimal->saveHeadcount(0);
    _vAnimal.emplace_back(pAnimal);
    int ageGroup = pAnimal->getGroupOfAge();
    if (ageGroup != enumAnimal::newBorn and healthState < enumAnimal::Ih){
        int ageGroupIndex;
        if (ageGroup == enumAnimal::cow){
            int parity = pAnimal->getParity();
            int boundedParity = (parity > iParams._nbStagesOfLactation) ? iParams._nbStagesOfLactation : parity;
            ageGroupIndex = iParams.vAgeThres.size()-1 + boundedParity - 1;
        } else {
            ageGroupIndex = ageGroup-1;
        }
        vvNbPerGrpPerState[ageGroupIndex][healthState] += 1;
    }
}

void Herd::initializeAnimals(std::vector<int>& vHeadcount, std::vector<std::tuple<int, int>>& vtAgeAndStateOfInfectedAnimals){
    Parameters& iParams = Parameters::getInstance();
    std::vector<std::vector<int>> vvNbPerGrpPerState(iParams.vAgeThres.size()-1 + iParams._nbStagesOfLactation, std::vector<int>(enumAnimal::Ih));
    for (auto tAgeAndState : vtAgeAndStateOfInfectedAnimals){
        createAnAnimal(std::get<0>(tAgeAndState), std::get<1>(tAgeAndState), vvNbPerGrpPerState);
    }
    for (auto i = 0; i < vHeadcount.size(); ++i){
        for (auto j = 0; j < vHeadcount[i]; ++j){
            createAnAnimal(i, enumAnimal::S, vvNbPerGrpPerState);
        }
    }
    _iHerdState.update(0);
    computeAnimalInfectedProp();
    setStatus();
    Metapop::getInstance().update(_status, vvNbPerGrpPerState);
}

void Herd::linkOffspringsToDam(enumAnimal::AgeGroup minAgeGroup) {
    std::vector<int> listDamIndex = {};
    std::vector<int> listOffspringIndex = {};
    int countDam = 0;
    int countOffspring = 0;
    int index = 0;
    for (auto animal : _vAnimal) {
        if (animal->getGroupOfAge() == enumAnimal::cow) {
            listDamIndex.emplace_back(index);
            countDam++;
        }
        index++;
    }
    index = 0;
    for (auto animal : _vAnimal) {
        if (animal->getGroupOfAge() <= minAgeGroup) {
            listOffspringIndex.emplace_back(index);
            countOffspring++;
        }
        index++;
    }
    for (int i=0; i < listOffspringIndex.size(); i++) {
        if (listDamIndex.size() != 0) {
            int damIndex = 0 + Utils::uniform(listDamIndex.size());
            _vAnimal[listOffspringIndex[i]]->setDamPtr(_vAnimal[listDamIndex[damIndex]].get());
            _vAnimal[listDamIndex[damIndex]]->addNewOffspring(_vAnimal[listOffspringIndex[i]].get());
            listDamIndex.erase(listDamIndex.begin()+damIndex);
        }
    }
    for (std::shared_ptr<Animal>& animal : _vAnimal) {
        if (animal->getOffspringNumber() > 1){
            std::cout << "ERROR : too many offsprings for this cow => " << animal->toString() << " -> " << animal->getOffspringNumber() << std::endl;
        }
    }
}

void Herd::updateRates(std::vector<float> vRatesPerGroup){
    //vRatesPerGroup should be of size 9: death rate at birth, death rate age=0-1 weeks, death rate age=2-8 weeks, death rate age=9-129, culling rate per parity (x5)
    Parameters& iParams = Parameters::getInstance();
    if (vRatesPerGroup.size() < 1)
        throw std::runtime_error("ERROR: no rate in rates file");
    _deathRateBirth = vRatesPerGroup[0];
    //from daily rates to weekly rates to probabilities
    std::vector<double> vWeeklyRates;
    for (auto rate : vRatesPerGroup){
        //first rate is transformed, but won't need it => it's _deathRateBirth (not transformed)
        vWeeklyRates.push_back(1 - std::exp(-(1 - std::pow((1-rate), (iParams.daysNbPerYear / static_cast<float>(iParams.weeksNbPerYear))))));
    }
    // Death-rate for all ages, except lactating stages
    if (vRatesPerGroup.size() < iParams.vWeeksNbPerDeathRate.size()+1)
        throw std::runtime_error("ERROR: vWeeksNbPerDeathRate and rates file don't fit");
    _vDeathRate.clear();
    for (auto i = 0; i < iParams.vWeeksNbPerDeathRate.size(); ++i){
        _vDeathRate.insert(_vDeathRate.end(), iParams.vWeeksNbPerDeathRate[i], vWeeklyRates[i+1]);
    }
    // Culling rates for lactating stages
    iParams.categoriesNbForCows = vRatesPerGroup.size() - (iParams.vWeeksNbPerDeathRate.size()+1);
    if (iParams.categoriesNbForCows < 1)
        throw std::runtime_error("ERROR: no culling rate in rates file");
    _vCullingRate.clear();
    for (auto i = iParams.vWeeksNbPerDeathRate.size()+1; i < vRatesPerGroup.size(); ++i){
        _vCullingRate.push_back(vWeeklyRates[i]);
    }
}

void Herd::initializeRates(std::vector<int>& vBirthsPerStep, std::vector<float>& vCullingRatesPerYearPerGroup){
    Parameters& iParams = Parameters::getInstance();
    _vBirthsNbPerStep = vBirthsPerStep;
    _vvCullingRatesPerYearPerGroup.resize(iParams.yearsNb);
    int ageGroupsNb = vCullingRatesPerYearPerGroup.size() / iParams.yearsNb;
    for (auto i = 0; i < iParams.yearsNb; ++i){
        _vvCullingRatesPerYearPerGroup[i].assign(vCullingRatesPerYearPerGroup.begin() + i*ageGroupsNb, vCullingRatesPerYearPerGroup.begin() + (i+1)*ageGroupsNb);
    }
    updateRates(_vvCullingRatesPerYearPerGroup[0]);
}

void Herd::updateEachYear(int yearNum){
    updateRates(_vvCullingRatesPerYearPerGroup[yearNum]);
}

void Herd::computeAnimalInfectedProp(){
    Parameters& iParams = Parameters::getInstance();
    int positiveIhNb = 0;
    std::vector<int> v2yearsOldNbPerHealthState(enumAnimal::Ih+1, 0);
    for (auto pAnimal : _vAnimal){
        if (pAnimal->getAge() >= iParams.weeksNbPerYear * 2){
            v2yearsOldNbPerHealthState[pAnimal->getHealthStatus()] += 1;
            if (pAnimal->getHealthStatus() == enumAnimal::Ih and Utils::bernoulli(iParams._testSensitivityIh)){
                positiveIhNb += 1;
                if (iParams.areDetectedIhCulledEarlier){
                    pAnimal->setCullingProbaIh(iParams.cullingProbaOfDetectedIh);
                }
            }
        }
    }
    _iHerdState._vPositiveIhtot[iParams._currentTime] = positiveIhNb;
    int twoYearsOldNb = std::accumulate(v2yearsOldNbPerHealthState.begin(), v2yearsOldNbPerHealthState.end(), 0);
    int positiveItNb = Utils::binomial(v2yearsOldNbPerHealthState[enumAnimal::It], iParams._testSensitivityIt);
    int positiveIlNb = Utils::binomial(v2yearsOldNbPerHealthState[enumAnimal::Il], iParams._testSensitivityIl);
    int positiveImNb = Utils::binomial(v2yearsOldNbPerHealthState[enumAnimal::Im], iParams._testSensitivityIm);
    int positiveSNb = Utils::binomial(v2yearsOldNbPerHealthState[enumAnimal::S], 1 - iParams._testSpecificity); //false positive
    _iHerdState._vPositiveTwoYearsOld[iParams._currentTime] = positiveItNb + positiveIlNb + positiveImNb + positiveIhNb + positiveSNb;
    positiveItNb = std::round(positiveItNb / iParams._testSensitivityIt);
    positiveIlNb = std::round(positiveIlNb / iParams._testSensitivityIl);
    positiveImNb = std::round(positiveImNb / iParams._testSensitivityIm);
    positiveIhNb = std::round(positiveIhNb / iParams._testSensitivityIh);
    _infectedProp = (positiveItNb + positiveIlNb + positiveImNb + positiveIhNb + positiveSNb) / float(twoYearsOldNb);
}

void Herd::setStatus(){
    Parameters& iParams = Parameters::getInstance();
    for (auto i = 0; i < iParams.vShedPropThres.size(); i++){
        if (_infectedProp <= iParams.vShedPropThres[i]){
            _status = i;
            if (iParams.useCalfExpoForSpecHerds and _status == iParams.specHerdForCalfExpo){
                _weanedExposure = iParams.calfExpoForSpecHerds;
                _unweanedExposure = iParams.calfExpoForSpecHerds;
            }
            break;
        }
    }
    _iHerdState._vHealthStates.push_back(_status);
    if (iParams.useHerdHistory){
        _status = eA;
        for (auto i : {-1, -2, -3}){
            if (_iHerdState._vHealthStates.end()[i] != eA){
                _status = eB;
                break;
            }
        }
        if (iParams.useCalfExpoForSelectedHerds and _status == eA){
            _weanedExposure = iParams.calfExpoForSelectedHerds;
            _unweanedExposure = iParams.calfExpoForSelectedHerds;
        }
        _iHerdState._vStatus.push_back(_status);
    }
}

int Herd::getStatus(){
    return _status;
}

void Herd::addPendingMoves(std::vector<MovementUnit>& vPendingMoves){
    Parameters& iParams = Parameters::getInstance();
    for (auto iMove : vPendingMoves){
        if (iParams.useHerdHistory and _status == eA and iParams.usePurchaseProba and Utils::bernoulli(iParams.probaToPurchaseS)){
            iMove.pAnimal->setHealthStatus(enumAnimal::S);
        }
        iMove.pAnimal->setExitReason(enumAnimal::noExit);
        int ageGroup = iMove.pAnimal->getGroupOfAge();
        iMove.pAnimal->setShPtrAgegroup(getAnimalAgeGroup(ageGroup));
        iMove.pAnimal->setHerd(this);
        _vAnimal.emplace_back(iMove.pAnimal);
    }
    vPendingMoves.clear();
}

void Herd::sendMoves(std::vector<MovementUnitFromData>& vMovesToSend, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside){
    Parameters& iParams = Parameters::getInstance();
    for (auto iMove : vMovesToSend){
        bool isAgeToBeChanged = false;
        int animalPosition = -1;
        if (iMove.age >= iParams._ageFirstCalving){
            int wantedParity = iMove.age - iParams._ageFirstCalving +1;
            std::vector<int> vPosOfAnimalWithSameParity;
            if (iMove.age == iParams._ageFirstCalving + iParams._nbStagesOfLactation -1){
                for (auto i = 0; i < _vAnimal.size(); ++i){
                    if (_vAnimal[i]->getParity() >= iParams._nbStagesOfLactation){
                        vPosOfAnimalWithSameParity.push_back(i);
                    }
                }
            } else {
                for (auto i = 0; i < _vAnimal.size(); ++i){
                    if (_vAnimal[i]->getParity() == wantedParity){
                        vPosOfAnimalWithSameParity.push_back(i);
                    }
                }
            }
            if (vPosOfAnimalWithSameParity.size() > 0){
                animalPosition = vPosOfAnimalWithSameParity[Utils::uniform(vPosOfAnimalWithSameParity.size())];
            } else {
                if (iMove.destinationId != iParams.outsideMetapop){
                    int wantedAge = (iMove.age - iParams._ageFirstCalving) * iParams.weeksNbPerYear + iParams._ageFirstCalving;
                    std::shared_ptr<Animal> pAnimal = std::make_shared<Animal>(nullptr, wantedAge, enumAnimal::S, wantedParity);
                    mvPendingMovesFromOutside[iMove.destinationId].emplace_back(MovementUnit(iParams.outsideMetapop, -1, iMove.breed, pAnimal, eNA));
                }
                continue;
            }
        } else {
            int ageUpperLimit;
            int ageLowerLimit;
            for (auto i = 0; i < iParams.vAgeThresForMoves.size(); ++i){
                if (iMove.age < iParams.vAgeThresForMoves[i]){
                    ageUpperLimit = iParams.vAgeThresForMoves[i];
                    ageLowerLimit = (i > 0) ? iParams.vAgeThresForMoves[i-1] : 0;
                    break;
                }
            }
            std::vector<int> vPosOfAnimalWithSameAge;
            std::vector<int> vPosOfAnimalInSameAgeGroup;
            for (auto i = 0; i < _vAnimal.size(); ++i){
                int ageCurrentAnimal = _vAnimal[i]->getAge();
                if (ageCurrentAnimal == iMove.age){
                    vPosOfAnimalWithSameAge.push_back(i);
                } else if (ageCurrentAnimal >= ageLowerLimit and ageCurrentAnimal < ageUpperLimit){
                    vPosOfAnimalInSameAgeGroup.push_back(i);
                }
            }
            if (vPosOfAnimalWithSameAge.size() > 0){
                animalPosition = vPosOfAnimalWithSameAge[Utils::uniform(vPosOfAnimalWithSameAge.size())];
            } else if (vPosOfAnimalInSameAgeGroup.size() > 0){
                isAgeToBeChanged = true;
                animalPosition = vPosOfAnimalInSameAgeGroup[Utils::uniform(vPosOfAnimalInSameAgeGroup.size())];
            } else {
                if (iMove.destinationId != iParams.outsideMetapop){
                    std::shared_ptr<Animal> pAnimal = std::make_shared<Animal>(nullptr, iMove.age, enumAnimal::S, 0);
                    mvPendingMovesFromOutside[iMove.destinationId].emplace_back(MovementUnit(iParams.outsideMetapop, -1, iMove.breed, pAnimal, eNA));
                }
                continue;
            }
        }
        auto pAnimal = _vAnimal[animalPosition];
        auto pDam = pAnimal->getDamPtr();
        if (pDam != nullptr) {
            pDam->deleteOneOffspring(pAnimal.get());
            pAnimal->deleteDamPtr();
        }
        if (isAgeToBeChanged){
            pAnimal->setAge(iMove.age);
        }
        pAnimal->clearOffspringMap();
        pAnimal->setAgeGroupFromAgeGroupPointer();
        pAnimal->setShPtrAgegroup(nullptr);
        pAnimal->setHerd(nullptr);
        mvOutPendingMoves[iMove.destinationId].emplace_back(MovementUnit(_id, _status, iMove.breed, pAnimal, eNotRewired));
        _vAnimal.erase(_vAnimal.begin() + animalPosition);
    }
}

void Herd::runDynamics(int t){
    Parameters& iParams = Parameters::getInstance();
    updateParams();
    _iMapShedding.reset();
    updateSheddingCalvingExitingAging(t);
    deleteExitAnimals();
    transferTmpAnimals();
    _iMapShedding.update();
    _iMapInEnv.update(t, _iMapShedding, _iHerdState, _startGrazing, _endGrazing);
    _iHerdState.updateMilkProd(t, _iMapShedding);
    updateTransitionInfection(t);
    _iHerdState.update(t);
    if (not iParams.useAnnualPrev or (t % iParams.weeksNbPerYear == _weekToUpdateState)){
        computeAnimalInfectedProp();
        setStatus();
    }
    Metapop::getInstance().update(_status, _vvCurrentNbPerGrpPerState);
    if (isPersistent(t)){
        _iHerdState.updatePersistence(t);
    }
}

bool Herd::isPersistent(int t){
    if (_iHerdState.infectedRemaining(t)){
        return true;
    }
    return false;
}

void Herd::updateParams(){
    Parameters& iParams = Parameters::getInstance();
    if ((iParams._weekNumber >= _startGrazing) && (iParams._weekNumber <= _endGrazing)) {
        _grazing = true;
    } else {
        _grazing = false;
    }
}

void Herd::updateCalving(std::shared_ptr<Animal>& pAnimal, int t) {
    int calfState = pAnimal->calving(t);
    if (calfState == 0){
        auto newAnimal = std::make_shared<Animal>(this, enumAnimal::newBorn, 0, enumAnimal::S, pAnimal->getHealthStatus());
        _vAnimal_tmp.emplace_back(newAnimal);
        _iHerdState._vBirthsOverTimeS[t]++;
        newAnimal.get()->setDamPtr(pAnimal.get());
        pAnimal.get()->addNewOffspring(newAnimal.get());
    } else if (calfState == 1){
        auto newAnimal = std::make_shared<Animal>(this, enumAnimal::newBorn, 0, enumAnimal::It, pAnimal->getHealthStatus());
        _vAnimal_tmp.emplace_back(newAnimal);
        _iHerdState._vBirthsOverTimeIt[t]++;
        _iHerdState._vAgeAtInfection_inutero[0]++;
        _iHerdState._vAgeAtInfection[0]++;
        _iHerdState._vTransmissionRoutes_inutero[t]++;
        _iHerdState._vIncidenceIt[t]++;
        newAnimal.get()->setDamPtr(pAnimal.get());
        pAnimal.get()->addNewOffspring(newAnimal.get());
    }
}

void Herd::updateSheddingCalvingExitingAging(int t) {
    Parameters& iParams = Parameters::getInstance();
    int nbOfBirths = _vBirthsNbPerStep[t-1];
    std::vector<int> vPosOfAnimalWhoCanCalve;
    for (auto i = 0; i < _vAnimal.size(); ++i){
        auto pAnimal = _vAnimal[i];
        pAnimal->shedding();
        pAnimal->exitAging(t);
        if (pAnimal->getTimeBeforeNextCalving() == 0 and pAnimal->getAge() >= iParams._ageFirstCalving -1){
            vPosOfAnimalWhoCanCalve.push_back(i);
        }
    }
    if (nbOfBirths > vPosOfAnimalWhoCanCalve.size()){
        for (auto i = 0; i < nbOfBirths-vPosOfAnimalWhoCanCalve.size(); ++i){
            auto newAnimal = std::make_shared<Animal>(this, 0, enumAnimal::S, 0);
            _vAnimal_tmp.emplace_back(newAnimal);
            _iHerdState._vBirthsOverTimeS[t]++;
        }
        nbOfBirths = vPosOfAnimalWhoCanCalve.size();
    }
    int animalPosInHerdVector;
    int animalPosInCalvingVector;
    for (auto i = 0; i < nbOfBirths; ++i){
        animalPosInCalvingVector = Utils::uniform(vPosOfAnimalWhoCanCalve.size());
        animalPosInHerdVector = vPosOfAnimalWhoCanCalve[animalPosInCalvingVector];
        updateCalving(_vAnimal[animalPosInHerdVector], t);
        vPosOfAnimalWhoCanCalve.erase(vPosOfAnimalWhoCanCalve.begin() + animalPosInCalvingVector);
    }
    _iHerdState._vHeadcountBuilding[t] = getNumberOfAnimalsInBuilding();
    _iHerdState._vHeadcountPasture[t] = getNumberOfAnimalsInPasture();
}

void Herd::resetNbPerGrpPerState(){
    Parameters& iParams = Parameters::getInstance();
    int ageGroupsNb = iParams.vAgeThres.size()-1 + iParams._nbStagesOfLactation;
    int healthStatesNb = enumAnimal::Ih;
    _vvCurrentNbPerGrpPerState.assign(ageGroupsNb, std::vector<int>(healthStatesNb, 0));
}

void Herd::updateTransitionInfection(int t) {
    Parameters& iParams = Parameters::getInstance();
    resetNbPerGrpPerState();
    for (std::shared_ptr<Animal>& pAnimal : _vAnimal) {
        if (!pAnimal->isExit()) {
            pAnimal->transition(t);
            pAnimal->updateConsumedInfectedColostrum();
            if (pAnimal->getAge() < iParams._durSusceptible) {
                pAnimal->infection(t);
            }
            pAnimal->saveHeadcount(t);
            int ageGroup = pAnimal->getGroupOfAge();
            int healthState = pAnimal->getHealthStatus();
            if (ageGroup != enumAnimal::newBorn and healthState < enumAnimal::Ih){
                int ageGroupIndex;
                if (ageGroup == enumAnimal::cow){
                    int parity = pAnimal->getParity();
                    int boundedParity = (parity > iParams._nbStagesOfLactation) ? iParams._nbStagesOfLactation : parity;
                    ageGroupIndex = iParams.vAgeThres.size()-1 + boundedParity - 1;
                } else {
                    ageGroupIndex = ageGroup-1;
                }
                _vvCurrentNbPerGrpPerState[ageGroupIndex][healthState] += 1;
            }
        }
    }
}

void Herd::deleteExitAnimals() {
    for (auto animal = _vAnimal.begin(); animal != _vAnimal.end();) {
        if ((*animal)->isExit()) {
            auto dam = (*animal)->getDamPtr();
            if (dam != NULL) {
                dam->deleteOneOffspring((*animal).get());
            }
            (*animal)->clearOffspringMap();
            animal = _vAnimal.erase(animal);
        } else {
            animal++;
        }
    }
}

void Herd::transferTmpAnimals() {
	for (auto animal_tmp = _vAnimal_tmp.begin(); animal_tmp != _vAnimal_tmp.end(); animal_tmp++) {
		auto animal(animal_tmp);
		_vAnimal.push_back((*animal));
	}
	clearHerdTmp();
}

std::vector<int> Herd::getHeadcount() {
	std::vector<int> headcount = {
			// SR, T, L, Is, Ic
			0, 0, 0, 0, 0, // Calf new born
			0, 0, 0, 0, 0, // Calf un weaned
			0, 0, 0, 0, 0, // Calf weaned IN
			0, 0, 0, 0, 0, // calf weaned OUT
			0, 0, 0, 0, 0, // Young heifer
			0, 0, 0, 0, 0, // Heifer
			0, 0, 0, 0, 0, // Cow P1
			0, 0, 0, 0, 0, // Cow P2
			0, 0, 0, 0, 0, // Cow P3
			0, 0, 0, 0, 0, // Cow P4
			0, 0, 0, 0, 0, // Cow P5+
	};
	std::vector<std::string> groupAge = {"New born", "Unweaned", "Weaned IN", "Weaned OUT", "Young heifer", "Heifer", "CowP1", "CowP2", "CowP3", "CowP4", "CowP5p"};
	for (const auto& animal : _vAnimal) {
		int indexGroup=0;
		switch (animal->getGroupOfAge()) {
			case enumAnimal::newBorn: indexGroup = 0; break;
			case enumAnimal::unweanedCalf: indexGroup = 1*5; break;
			case enumAnimal::weanedInCalf: indexGroup = 2*5; break;
			case enumAnimal::weanedOutCalf: indexGroup = 3*5; break;
			case enumAnimal::youngHeifer: indexGroup = 4*5; break;
			case enumAnimal::heifer: indexGroup = 5*5; break;
			case enumAnimal::cow:
				switch (animal->getParity()) {
					case 1: indexGroup = 6*5; break;
					case 2: indexGroup = 7*5; break;
					case 3: indexGroup = 8*5; break;
					case 4: indexGroup = 9*5; break;
					default: indexGroup = 10*5;
				}
		}
		switch (animal->getHealthStatus()) {
			case enumAnimal::S: indexGroup+= 0; break;
			case enumAnimal::It: indexGroup+= 1; break;
			case enumAnimal::Il: indexGroup+= 2; break;
			case enumAnimal::Im: indexGroup+= 3; break;
			case enumAnimal::Ih: indexGroup+= 4; break;
		}
		headcount[indexGroup]++;
	}
	return headcount;
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <map>

class AnimalAgeGroup;
class Herd;

#include "../Parameters.h"
#include "../Utils.h"
#include "RepetitionHerdState.h"
#include "map/MapShedding.h"
#include "map/MapInEnvironments.h"

class Animal {
private:
    Herd* _pHerd = nullptr;
    short int _age;
    std::shared_ptr<AnimalAgeGroup> _shptrAgeGroup = nullptr;
    int _ageGroup;
    Animal* _ptrDam = nullptr;
    std::map<Animal*, Animal*> _mptrOffspring;
    short int _parity = 0;
    short int _timeBeforeNextCalving = 0;
    int _healthStatus;
    int _damHealthStatus = enumAnimal::S;
    
    float _consumedInfectedColostrum = 0;
    short int _timeInIh = 0;
    int _exitReason = enumAnimal::noExit;
    double _cullingProbaIh = 1 - std::exp(-(1./26.));

public:
    Animal(Herd* pHerd, int age, int hs, int parity);
    Animal(Herd* pHerd, int ag, short int timeInAgegroup, int hs, int dam_hs);
    ~Animal();

    Herd* getHerd(){return _pHerd;}
    RepetitionHerdState& getHerdState();
    MapShedding& getMapShedding();
    MapInEnvironments& getMapInEnv();
    short int getAge() {return _age;}
    int getHealthStatus() {return _healthStatus;}
    int getDamHealthStatus() {return _damHealthStatus;}

    float getConsumedInfectedColostrum() {return _consumedInfectedColostrum;}
    short int getTimeInIc() {return _timeInIh;}
    int getExitReason() {return _exitReason;}
    int getGroupOfAge();
    short int getParity() {return _parity;}
    short int getTimeBeforeNextCalving() {return _timeBeforeNextCalving;}
    double getCullingProbaIh() {return _cullingProbaIh;}

    bool isExit() {
        if (_exitReason != enumAnimal::noExit) {
            return true;
        } else {
            return false;
        }
    }
    void setAge(int a) {_age = a;};
    void setAgeGroupFromAgeGroupPointer() {_ageGroup = getGroupOfAge();};
    void setShPtrAgegroup(std::shared_ptr<AnimalAgeGroup> shptr_ag) {_shptrAgeGroup = shptr_ag;};
    void setHealthStatus(int hs) {_healthStatus = hs;};
    void setHerd(Herd* pHerd) {_pHerd = pHerd;};
    void incrementAge() {_age++;}
    void incrementParity() {_parity++;}
    void initializeTimeBeforeNextCalving();
    void updateTimeBeforeNextCalving();
    void setConsumedInfectedColostrum(float infectedColostrum) {_consumedInfectedColostrum = infectedColostrum;}
    void setExitReason(enumAnimal::AnimalExitReason exitReason) {_exitReason = exitReason;};
    void setCullingProbaIh(double proba) {_cullingProbaIh = proba;}

    void setDamPtr(Animal* animal);
    Animal* getDamPtr();
    void deleteDamPtr();
    void addNewOffspring(Animal* animal);
    void deleteOneOffspring(Animal* animal);
    int getOffspringNumber() {
        return _mptrOffspring.size();
    }
    void clearOffspringMap();
    
    void saveHeadcount (int t);
    int calving(int t);			// return value according to possible calving
    void exitAging(int t);			// age++ and change of age group
    void shedding();							// update amount of bacteria shed in environments
    void updateConsumedInfectedColostrum();					// update the value for new born calves
    void infection(int t);
    void transition(int t);		// evolution of health status through time
    std::string toString();
};


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Heifer.h"
#include "../Animal.h"

void Heifer::saveHeadcount(int t, RepetitionHerdState& rhs, Animal& iAnimal) {
	rhs._vHeifers[t]++;
	switch (iAnimal.getHealthStatus()) {
		case enumAnimal::S: rhs._vRtot[t]++;break;
		case enumAnimal::It: rhs._vIttot[t]++;break;
		case enumAnimal::Il: rhs._vIltot[t]++;break;
		case enumAnimal::Im: rhs._vImtot[t]++;break;
		case enumAnimal::Ih: rhs._vIhtot[t]++;break;
	}
    if (iAnimal.getAge() > Parameters::getInstance().weeksNbPerYear * 2){
        rhs._vTwoYearsOld[t]++;
        if (iAnimal.getHealthStatus() != enumAnimal::S){
            rhs._vInfectedTwoYearsOld[t]++;
        }
    }
}

int Heifer::calving(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	double infinutero = 0;
    switch (animal.getHealthStatus()) {
        case 0: infinutero = 0; break;
        case 1: infinutero = param._infinuteroIt; break;
        case 2: infinutero = param._infinuteroIl; break;
        case 3: infinutero = param._infinuteroIm; break;
        case 4: infinutero = param._infinuteroIh; break;
    }
    animal.incrementParity();
    if (Utils::bernoulli(1 - animal.getHerd()->getDeathRateBirth())) {
        animal.initializeTimeBeforeNextCalving();
        if (animal.getHealthStatus() == enumAnimal::S) {
            return 0;
        } else {
            if (Utils::bernoulli(infinutero)) {
                return 1;
            } else {
                return 0;
            }
        }
    } else {
        return -1;
    }
}

void Heifer::exitAging(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
    animal.updateTimeBeforeNextCalving();
    if (animal.getExitReason() == enumAnimal::noExit) {
        switch (animal.getHealthStatus()) {
            case enumAnimal::S : if(!Utils::bernoulli(1 - animal.getHerd()->getDeathRates()[animal.getAge()])) {animal.setExitReason(enumAnimal::death);} break;
            case enumAnimal::It : if(!Utils::bernoulli(1 - animal.getHerd()->getDeathRates()[animal.getAge()])) {animal.setExitReason(enumAnimal::death);} break;
            case enumAnimal::Il : if(!Utils::bernoulli(1 - animal.getHerd()->getDeathRates()[animal.getAge()])) {animal.setExitReason(enumAnimal::death);} break;
            case enumAnimal::Im : if(!Utils::bernoulli((1 - animal.getHerd()->getDeathRates()[animal.getAge()])*(1 - param._deathRateIm))) {animal.setExitReason(enumAnimal::death);} break;
            case enumAnimal::Ih : if(!Utils::bernoulli((1 - animal.getHerd()->getDeathRates()[animal.getAge()])*(1 - param._deathRateIh))) {animal.setExitReason(enumAnimal::death);} break;
        }
    }
    if (animal.getExitReason() == enumAnimal::noExit) {
        animal.incrementAge();
        if (animal.getAge() >= param._ageFirstCalving) {
            if (animal.getHealthStatus() == enumAnimal::It) {
                animal.setHealthStatus(enumAnimal::Il);
            }
            animal.setShPtrAgegroup(animal.getHerd()->getAnimalAgeGroup(enumAnimal::cow));
        }
    }
}

void Heifer::shedding(MapShedding& ms, int hs, short int& parity) {
	Parameters& param = Parameters::getInstance();
	if (hs == enumAnimal::It) {
		ms._QtyIt_g += param._qtyFaecesAd * pow(10, 4) * 100 * Utils::beta(param._alphaFaecesIt, param._betaFaecesIt);
	} else if (hs == enumAnimal::Im) {
		ms._QtyIm_g += param._qtyFaecesAd * pow(10, (4+10 * Utils::beta(param._alphaFaecesIm, param._betaFaecesIm)));
	} else if (hs == enumAnimal::Ih) {
		ms._QtyIh_g += param._qtyFaecesAd * pow(10, (8+10 * Utils::beta(param._alphaFaecesIh, param._betaFaecesIh)));
	}
}

void Heifer::infection(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	// Infection by Local environments in and out of the grazing period
	if (animal.getHealthStatus() == enumAnimal::S && param._localTR == true) {
		if (animal.getHerd()->isGrazingPeriod()) {
			if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infGrazing * animal.getMapInEnv()._vEnvOutside_3[t] / animal.getHerd()->getNumberOfAnimals(enumAnimal::heifer) / param._infectiousDose))) {
				animal.setHealthStatus(enumAnimal::It);
				animal.getHerdState()._vIncidenceIt[t]++;
			}
		// Out of grazing period
		} else {
			if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infLocalEnv * animal.getMapInEnv()._vEnvInside_4[t] / animal.getHerd()->getNumberOfAnimals(enumAnimal::heifer) / param._infectiousDose))) {
				animal.setHealthStatus(enumAnimal::It);
				animal.getHerdState()._vIncidenceIt[t]++;
			}
		}
	}
	// Infection by General environments
	if (animal.getHealthStatus() == enumAnimal::S) {
		if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infGeneralEnv * animal.getMapInEnv()._vEnvGeneral[t] / animal.getHerd()->getNumberOfAnimalsInBuilding() / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
}

void Heifer::transition(int t, Animal& animal) {
	int hs = animal.getHealthStatus();
	if (hs != enumAnimal::S and hs != enumAnimal::Ih) {
		Parameters& param = Parameters::getInstance();
		switch(hs) {
			case enumAnimal::It:
				if (Utils::bernoulli(1./param._durShedding)) {
					animal.setHealthStatus(enumAnimal::Il);
					animal.getHerdState()._vIncidenceIl[t]++;
				}
				break;
			case enumAnimal::Il:
				if (Utils::bernoulli(1./param._durIl)) {
					animal.setHealthStatus(enumAnimal::Im);
					animal.getHerdState()._vIncidenceIm[t]++;
				}
				break;
			case enumAnimal::Im:
				if (Utils::bernoulli(1./param._durIm)) {
					animal.setHealthStatus(enumAnimal::Ih);
					animal.getHerdState()._vIncidenceIh[t]++;
				}
				break;
		}
	}
}

enumAnimal::AgeGroup Heifer::getAgeGroup() {
	return enumAnimal::heifer;
}

std::string Heifer::getAgeGroupStr() {
	return std::string("Heifer");
}

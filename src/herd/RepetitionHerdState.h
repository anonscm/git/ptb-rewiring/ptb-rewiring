
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <vector>

#include "../Parameters.h"
#include "map/MapShedding.h"

class RepetitionHerdState {
public:
    RepetitionHerdState();

    std::vector<int> _vCalvesNewBorn;
    std::vector<int> _vCalvesUnWeaned;
    std::vector<int> _vCalvesWeanedIn;
    std::vector<int> _vCalvesWeanedOut;
    std::vector<int> _vYoungHeifers;
    std::vector<int> _vHeifers;
    std::vector<int> _vCows;
    std::vector<int> _vTwoYearsOld;
    std::vector<int> _vHeadcount;
    std::vector<int> _vHeadcountBuilding;
    std::vector<int> _vHeadcountPasture;

    std::vector<int> _vBirthsOverTimeS;
    std::vector<int> _vBirthsOverTimeIt;
    std::vector<int> _vExitCow;
    std::vector<int> _vExitCowIh;

    std::vector<int> _vPersistence;

    std::vector<int> _vStot;
    std::vector<int> _vRtot;
    std::vector<int> _vIttot;
    std::vector<int> _vIltot;
    std::vector<int> _vImtot;
    std::vector<int> _vIhtot;
    std::vector<int> _vPositiveIhtot;

    std::vector<int> _vInfected;
    std::vector<int> _vInfectious;
    std::vector<int> _vAffected;
    std::vector<int> _vInfectedAdults;
    std::vector<int> _vPositiveTwoYearsOld;
    std::vector<int> _vInfectedTwoYearsOld;
    std::vector<int> _vIlImIh;
    std::vector<int> _vHealthStates;
    std::vector<int> _vStatus;

    std::vector<double> _vMilkprodTot;
    std::vector<double> _vMilkprodTot_P1;
    std::vector<double> _vMilkprodTot_P2;
    std::vector<double> _vMilkprodTot_P3;
    std::vector<double> _vMilkprodTot_P4;
    std::vector<double> _vMilkprodTot_P5;

    std::vector<double> _vMilkprodTotR;
    std::vector<double> _vMilkprodTotR_P1;
    std::vector<double> _vMilkprodTotR_P2;
    std::vector<double> _vMilkprodTotR_P3;
    std::vector<double> _vMilkprodTotR_P4;
    std::vector<double> _vMilkprodTotR_P5;

    std::vector<double> _vMilkprodTotIt;
    std::vector<double> _vMilkprodTotIt_P1;
    std::vector<double> _vMilkprodTotIt_P2;
    std::vector<double> _vMilkprodTotIt_P3;
    std::vector<double> _vMilkprodTotIt_P4;
    std::vector<double> _vMilkprodTotIt_P5;

    std::vector<double> _vMilkprodTotIl;
    std::vector<double> _vMilkprodTotIl_P1;
    std::vector<double> _vMilkprodTotIl_P2;
    std::vector<double> _vMilkprodTotIl_P3;
    std::vector<double> _vMilkprodTotIl_P4;
    std::vector<double> _vMilkprodTotIl_P5;

    std::vector<double> _vMilkprodTotIm;
    std::vector<double> _vMilkprodTotIm_P1;
    std::vector<double> _vMilkprodTotIm_P2;
    std::vector<double> _vMilkprodTotIm_P3;
    std::vector<double> _vMilkprodTotIm_P4;
    std::vector<double> _vMilkprodTotIm_P5;

    std::vector<double> _vMilkprodTotIh;
    std::vector<double> _vMilkprodTotIh_P1;
    std::vector<double> _vMilkprodTotIh_P2;
    std::vector<double> _vMilkprodTotIh_P3;
    std::vector<double> _vMilkprodTotIh_P4;
    std::vector<double> _vMilkprodTotIh_P5;

    std::vector<int> _vAgeAtInfection;
    std::vector<int> _vAgeAtInfection_inutero;
    std::vector<int> _vAgeAtInfection_colostrum;
    std::vector<int> _vAgeAtInfection_milk;
    std::vector<int> _vAgeAtInfection_local;
    std::vector<int> _vAgeAtInfection_general;

    std::vector<int> _vTransmissionRoutes_inutero;
    std::vector<int> _vTransmissionRoutes_colostrum;
    std::vector<int> _vTransmissionRoutes_milk;
    std::vector<int> _vTransmissionRoutes_local;
    std::vector<int> _vTransmissionRoutes_general;

    std::vector<int> _vIncidenceIt;
    std::vector<int> _vIncidenceIl;
    std::vector<int> _vIncidenceIm;
    std::vector<int> _vIncidenceIh;

    std::vector<int> _vIncidenceCumulatedIt;
    std::vector<int> _vIncidenceCumulatedIl;
    std::vector<int> _vIncidenceCumulatedIm;
    std::vector<int> _vIncidenceCumulatedIh;

    void update(int tnow);
    void updatePersistence(int tnow);
    void updateMilkProd(int tnow,  const MapShedding& ms) {
        _vMilkprodTot[tnow] = ms._milkTot;
        _vMilkprodTot_P1[tnow] = ms._milkTot_P1;
        _vMilkprodTot_P2[tnow] = ms._milkTot_P2;
        _vMilkprodTot_P3[tnow] = ms._milkTot_P3;
        _vMilkprodTot_P4[tnow] = ms._milkTot_P4;
        _vMilkprodTot_P5[tnow] = ms._milkTot_P5;

        _vMilkprodTotR[tnow] = ms._milkTotR;
        _vMilkprodTotR_P1[tnow] = ms._milkTotR_P1;
        _vMilkprodTotR_P2[tnow] = ms._milkTotR_P2;
        _vMilkprodTotR_P3[tnow] = ms._milkTotR_P3;
        _vMilkprodTotR_P4[tnow] = ms._milkTotR_P4;
        _vMilkprodTotR_P5[tnow] = ms._milkTotR_P5;

        _vMilkprodTotIt[tnow] = ms._milkTotIt;
        _vMilkprodTotIt_P1[tnow] = ms._milkTotIt_P1;
        _vMilkprodTotIt_P2[tnow] = ms._milkTotIt_P2;
        _vMilkprodTotIt_P3[tnow] = ms._milkTotIt_P3;
        _vMilkprodTotIt_P4[tnow] = ms._milkTotIt_P4;
        _vMilkprodTotIt_P5[tnow] = ms._milkTotIt_P5;

        _vMilkprodTotIl[tnow] = ms._milkTotIl;
        _vMilkprodTotIl_P1[tnow] = ms._milkTotIl_P1;
        _vMilkprodTotIl_P2[tnow] = ms._milkTotIl_P2;
        _vMilkprodTotIl_P3[tnow] = ms._milkTotIl_P3;
        _vMilkprodTotIl_P4[tnow] = ms._milkTotIl_P4;
        _vMilkprodTotIl_P5[tnow] = ms._milkTotIl_P5;

        _vMilkprodTotIm[tnow] = ms._milkTotIm;
        _vMilkprodTotIm_P1[tnow] = ms._milkTotIm_P1;
        _vMilkprodTotIm_P2[tnow] = ms._milkTotIm_P2;
        _vMilkprodTotIm_P3[tnow] = ms._milkTotIm_P3;
        _vMilkprodTotIm_P4[tnow] = ms._milkTotIm_P4;
        _vMilkprodTotIm_P5[tnow] = ms._milkTotIm_P5;

        _vMilkprodTotIh[tnow] = ms._milkTotIh;
        _vMilkprodTotIh_P1[tnow] = ms._milkTotIh_P1;
        _vMilkprodTotIh_P2[tnow] = ms._milkTotIh_P2;
        _vMilkprodTotIh_P3[tnow] = ms._milkTotIh_P3;
        _vMilkprodTotIh_P4[tnow] = ms._milkTotIh_P4;
        _vMilkprodTotIh_P5[tnow] = ms._milkTotIh_P5;
    }
    void updateIncidenceCumulated();
    bool infectedRemaining(int tnow);
    void printData(int t);
};

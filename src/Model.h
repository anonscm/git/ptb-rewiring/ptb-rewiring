
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <fstream>
#include <stdexcept>
#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <math.h>
#include <assert.h>

#include "Parameters.h"
#include "Metapop.h"
#include "Utils.h"
#include "herd/AgeGroup/AnimalAgeGroup.h"
#include "herd/Animal.h"
#include "herd/Herd.h"
#include "herd/RepetitionHerdState.h"
#include "MovementUnit.h"
#include "rewiring/MovesRewiring.h"

class Model {
private:
    std::map<std::string, std::vector<int>> mvHeadcountPerHerdPerAgeAtStart;
    std::map<std::string, std::vector<int>> mvBirthsNbPerHerdPerStep;
    std::map<std::string, std::vector<float>> mvCullingRatesPerHerdPerYearPerGroup;
    std::map<int, std::vector<std::vector<std::vector<double>>>> mvvvPrevToRepetOfNbPerStatePerAge;
    std::map<int, std::vector<std::vector<double>>> mvvPrevToMeanNbPerStatePerAge;
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    std::vector<std::string> vHerdIds;
    std::vector<std::string> vHerdIdsToRewire;
    std::vector<std::string> vHerdIdsToInfect;
    std::vector<std::string> vSelectedHerdIds;
    std::vector<int> vInitPrev;
    std::map<std::string, std::vector<int>> mvHerdIdToInfectedNbAndPrevAndRepet;
    std::vector< std::map<std::string, std::vector<MovementUnitFromData>> > vmvMovements;
    std::map<std::string, std::vector<MovementUnit>> mvInPendingMoves;
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;

public:
    void loadDemography();
    void loadMovements();
    void loadHerdsToRewire();
    void loadSelectedHerds();
    void loadInitialInfection();
    void loadInitialPrevalence();
    void loadInitPrevMatrix();
    void initializeHerds();
    void initializeHerdsAccordingToStatus();
    void initializeHerdsAccordingToPrev();
    void infectRandomHerds();
    void sendMovesFromOutsideMetapop(int stepNum, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside);
    void saveMovements(std::string fileName, int stepNum);
    void saveHeadcountForComparisonWithObserved(int runNum);
    void saveResults(int runNum);
    void run();
};

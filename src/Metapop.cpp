
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Metapop.h"

Metapop* Metapop::s_instance = nullptr;

Metapop& Metapop::getInstance(){
    if (s_instance == nullptr){
        s_instance = new Metapop();
    }
    return *s_instance;
}

void Metapop::kill(){
    if (s_instance != nullptr){
        delete s_instance;
    }
    s_instance = nullptr;
}

Metapop::Metapop(){
    Parameters& iParams = Parameters::getInstance();
    ageGroupsNb = iParams.vAgeThres.size()-1 + iParams._nbStagesOfLactation;
    healthStatesNb = enumAnimal::Ih;
}

std::vector<double>& Metapop::getHealthStateProbas(int ageGroup){
    return _vvProbaPerGrpPerState[ageGroup];
}

std::vector<double>& Metapop::getHealthStateProbas(int ageGroup, int status){
    return _mvvPrevThresToProbaPerGrpPerState[status][ageGroup];
}

void Metapop::reset(){
    _vvNbPerGrpPerState.assign(ageGroupsNb, std::vector<int>(healthStatesNb, 0));
    _vvProbaPerGrpPerState.assign(ageGroupsNb, std::vector<double>(healthStatesNb, 0));
    for (auto i = 0; i < Parameters::getInstance().herdStatusNb; ++i){
        _mvvPrevThresToNbPerGrpPerState.emplace(i, std::vector<std::vector<int>>(ageGroupsNb, std::vector<int>(healthStatesNb)));
        _mvvPrevThresToProbaPerGrpPerState.emplace(i, std::vector<std::vector<double>>(ageGroupsNb, std::vector<double>(healthStatesNb)));
    }
}

void Metapop::update(int status, std::vector<std::vector<int>>& vvNbPerGrpPerState){
    for (auto i = 0; i < _vvNbPerGrpPerState.size(); ++i){
        Utils::accumulateInFirstVector(_vvNbPerGrpPerState[i], vvNbPerGrpPerState[i]);
        Utils::accumulateInFirstVector(_mvvPrevThresToNbPerGrpPerState[status][i], vvNbPerGrpPerState[i]);
    }
}

void Metapop::computeHealthStateProbaPerAgeGroup(){
    for (auto ageGroup = 0; ageGroup < _vvNbPerGrpPerState.size(); ++ageGroup){
        double N = std::accumulate(_vvNbPerGrpPerState[ageGroup].begin(), _vvNbPerGrpPerState[ageGroup].end(), 0.0);
        for (auto state = 0; state < _vvNbPerGrpPerState[ageGroup].size(); ++state){
            _vvProbaPerGrpPerState[ageGroup][state] = _vvNbPerGrpPerState[ageGroup][state] / N;
        }
    }
    for (auto ageGroup = 0; ageGroup < ageGroupsNb; ++ageGroup){
        double N = 0.;
        std::vector<int> vNbPerState(healthStatesNb);
        for (auto i = 0; i < Parameters::getInstance().herdStatusNb; ++i){
            N += std::accumulate(_mvvPrevThresToNbPerGrpPerState[i][ageGroup].begin(), _mvvPrevThresToNbPerGrpPerState[i][ageGroup].end(), 0.0);
            Utils::accumulateInFirstVector(vNbPerState, _mvvPrevThresToNbPerGrpPerState[i][ageGroup]);
            for (auto state = 0; state < vNbPerState.size(); ++state){
                _mvvPrevThresToProbaPerGrpPerState[i][ageGroup][state] = vNbPerState[state] / N;
            }
        }
    }
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include <UnitTest++.h>
#include <string>
#include <vector>
#include <memory>

#include "../rewiring/MovesRewiring.h"
#include "../rewiring/MovesMatrix.h"
#include "../MovementUnit.h"
#include "../herd/Animal.h"
#include "../herd/Herd.h"

class MockHerd : public Herd {
private:
    int _status;
public:
    MockHerd(std::string id, int status) : Herd(id){
        _status = status;
    };
    int getStatus(){
        return _status;
    }
};

struct ParametersFixture{
    ParametersFixture(){
        Parameters& iParams = Parameters::getInstance();
        iParams.vShedPropThres = {0.07, 0.21, 0.60, 1.0};
        iParams.vAgeThresForMoves = {10, 26, 52, 91, 101, 111, 121, 131, 132, 133, 134, 135, 136};
        iParams.outsideMetapop = "outside";
        iParams.useSelectedHerdsToRewire = false;
        iParams.useBreed = false;
        iParams.breedToRewire = 66;
    }
    ~ParametersFixture(){
        Parameters::kill();
    }
};

class MockHerdWithImp : public Herd {
private:
    float _sheddersProp;
    int _status;
    std::vector<int> _vHealthStates;
public:
    MockHerdWithImp(std::string id, float sheddersProp, int state1=99, int state2=99) : Herd(id){
        _sheddersProp = sheddersProp;
        _vHealthStates = {eA, eA, state1, state2};
    };
    void setStatus(){
        Parameters& iParams = Parameters::getInstance();
        for (auto i = 0; i < iParams.vShedPropThres.size(); i++){
            if (_sheddersProp <= iParams.vShedPropThres[i]){
                _status = i;
                break;
            }
        }
        _vHealthStates.push_back(_status);
        if (iParams.useHerdHistory){
            _status = eA;
            for (auto i : {-1, -2, -3}){
                if (_vHealthStates.end()[i] != eA){
                    _status = eB;
                    break;
                }
            }
        }
    }
    int getStatus(){
        return _status;
    }
};

TEST_FIXTURE(ParametersFixture, fillMatrices_global_oneMatrix_3lastStates){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    Parameters::getInstance().useHerdHistory = true;
    std::vector<std::string> vIds{"fake"};
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerdWithImp>("dest00", 0.06, eA, eA)); //sheddersProp will be used to define last status
    mpHerds.emplace("dest01", std::make_shared<MockHerdWithImp>("dest01", 0.20, eA, eB));
    mpHerds.emplace("dest02", std::make_shared<MockHerdWithImp>("dest02", 0.06, eC, eA));
    mpHerds.emplace("dest04", std::make_shared<MockHerdWithImp>("dest04", 0.99, eA, eA));
    mpHerds.emplace("dest05", std::make_shared<MockHerdWithImp>("dest05", 0.99, eC, eC));
    for (auto& it : mpHerds){
        it.second->setStatus();
    }
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eB, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eB, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eB, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eB, 66, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eB, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eB, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eB, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eB, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eB, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eB, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eB, 66, vpAnimal[24], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eB, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eB, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eB, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eB, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eB, 66, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[10]));
    expMovesMatrix[eA][eB].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[14]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[15]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[16]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source05", "dest00", vpAnimal[20]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source06", "dest00", vpAnimal[21]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source07", "dest00", vpAnimal[22]));
    expMovesMatrix[eB][eA].push_back(std::make_shared<MovementForMatrix>("source08", "dest00", vpAnimal[23]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[0].isEmpty());
    for (auto i = 2; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix == iMovesRewiring.getMatricesVector()[1]);
}

TEST_FIXTURE(ParametersFixture, fillMatrices_global_oneMatrix){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    Parameters::getInstance().useSelectedHerdsToRewire = true;
    std::vector<std::string> vIds{"dest00", "dest01", "dest02", "dest03", "dest04", "dest05", "dest06",
            "source00", "source01", "source02", "source03", "source04", "source05", "source06", "source07", "source08", "source09", "source10", "source11", "source12", "source13", "source14"};
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerd>("dest00", eA));
    mpHerds.emplace("dest01", std::make_shared<MockHerd>("dest01", eC));
    mpHerds.emplace("dest02", std::make_shared<MockHerd>("dest02", eC));
    mpHerds.emplace("dest04", std::make_shared<MockHerd>("dest04", eD));
    mpHerds.emplace("dest05", std::make_shared<MockHerd>("dest05", eD));
    mpHerds.emplace("dest06", std::make_shared<MockHerd>("dest06", eD));
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[10]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest06", vpAnimal[12]));
    expMovesMatrix[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[14]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[15]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[16]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest04", vpAnimal[18]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source05", "dest00", vpAnimal[20]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source06", "dest00", vpAnimal[21]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source07", "dest00", vpAnimal[22]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source08", "dest00", vpAnimal[23]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source09", "dest02", vpAnimal[24]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source10", "dest02", vpAnimal[25]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source11", "dest02", vpAnimal[26]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source12", "dest02", vpAnimal[27]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source13", "dest02", vpAnimal[28]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[0].isEmpty());
    for (auto i = 2; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix == iMovesRewiring.getMatricesVector()[1]);
}

TEST_FIXTURE(ParametersFixture, fillMatrices_global_oneMatrix_otherOrder){//changing order between destinations has no impact, but changing order between moves of a destination has an impact!
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    std::vector<std::string> vIds{"dest00", "fake"};    //it doesn't matter, because useSelectedHerdsToRewire = false
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerd>("dest00", eA));
    mpHerds.emplace("dest01", std::make_shared<MockHerd>("dest01", eC));
    mpHerds.emplace("dest02", std::make_shared<MockHerd>("dest02", eC));
    mpHerds.emplace("dest04", std::make_shared<MockHerd>("dest04", eD));
    mpHerds.emplace("dest05", std::make_shared<MockHerd>("dest05", eD));
    mpHerds.emplace("dest06", std::make_shared<MockHerd>("dest06", eD));
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0)); //change moves order for a destination
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 0)); //change destinations order
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[10]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7])); //only impact this move
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest06", vpAnimal[12]));
    expMovesMatrix[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[14]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[15]));
    expMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[16]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest04", vpAnimal[18]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source05", "dest00", vpAnimal[20]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source06", "dest00", vpAnimal[21]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source07", "dest00", vpAnimal[22]));
    expMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source08", "dest00", vpAnimal[23]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source09", "dest02", vpAnimal[24]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source10", "dest02", vpAnimal[25]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source11", "dest02", vpAnimal[26]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source12", "dest02", vpAnimal[27]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source13", "dest02", vpAnimal[28]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[0].isEmpty());
    for (auto i = 2; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix == iMovesRewiring.getMatricesVector()[1]);
}

TEST_FIXTURE(ParametersFixture, fillMatrices_global_oneMatrix_otherOrder_selectHerds){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    Parameters::getInstance().useSelectedHerdsToRewire = true;
    std::vector<std::string> vIds{"dest01", "dest02", "dest03", "dest04", "dest05", "dest06",
            "source00", "source01", "source02", "source03", "source04", "source05", "source06", "source07", "source08", "source09", "source10", "source11", "source12", "source13", "source14"};
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerd>("dest00", eA));
    mpHerds.emplace("dest01", std::make_shared<MockHerd>("dest01", eC));
    mpHerds.emplace("dest02", std::make_shared<MockHerd>("dest02", eC));
    mpHerds.emplace("dest04", std::make_shared<MockHerd>("dest04", eD));
    mpHerds.emplace("dest05", std::make_shared<MockHerd>("dest05", eD));
    mpHerds.emplace("dest06", std::make_shared<MockHerd>("dest06", eD));
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0)); //change moves order for a destination
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 0)); //change destinations order
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[10]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7])); //only impact this move
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest06", vpAnimal[12]));
    expMovesMatrix[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest04", vpAnimal[18]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source09", "dest02", vpAnimal[24]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source10", "dest02", vpAnimal[25]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source11", "dest02", vpAnimal[26]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source12", "dest02", vpAnimal[27]));
    expMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source13", "dest02", vpAnimal[28]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[0].isEmpty());
    for (auto i = 2; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix == iMovesRewiring.getMatricesVector()[1]);
}

TEST_FIXTURE(ParametersFixture, fillMatrices_global_oneMatrix_otherOrder_useBreed){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    Parameters::getInstance().useBreed = true;
    std::vector<std::string> vIds{"dest00", "fake"};    //it doesn't matter, because useSelectedHerdsToRewire = false
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerd>("dest00", eA));
    mpHerds.emplace("dest01", std::make_shared<MockHerd>("dest01", eC));
    mpHerds.emplace("dest02", std::make_shared<MockHerd>("dest02", eC));
    mpHerds.emplace("dest04", std::make_shared<MockHerd>("dest04", eD));
    mpHerds.emplace("dest05", std::make_shared<MockHerd>("dest05", eD));
    mpHerds.emplace("dest06", std::make_shared<MockHerd>("dest06", eD));
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[7], 0)); //change moves order for a destination
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 6, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 6, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 6, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 6, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 6, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 6, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 6, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 6, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 6, vpAnimal[20], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 6, vpAnimal[24], 0)); //change destinations order
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 6, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 6, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 6, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 6, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 6, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 6, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 6, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 6, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[0].isEmpty());
    for (auto i = 2; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix == iMovesRewiring.getMatricesVector()[1]);
}

TEST_FIXTURE(ParametersFixture, fillMatrices_global){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 9; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //health status could be different
    }
    vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 5, 3, 0));
    for (auto i = 10; i < 25; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 101, 0, 0)); //health status could be different
    }
    for (auto i = 25; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 60, 1, 0)); //health status could be different
    }
    std::vector<std::string> vIds{"dest00", "fake"};    //it doesn't matter, because useSelectedHerdsToRewire = false
    std::map<std::string, std::shared_ptr<Herd>> mpHerds;
    mpHerds.emplace("dest00", std::make_shared<MockHerd>("dest00", eA));
    mpHerds.emplace("dest01", std::make_shared<MockHerd>("dest01", eC));
    mpHerds.emplace("dest02", std::make_shared<MockHerd>("dest02", eC));
    mpHerds.emplace("dest04", std::make_shared<MockHerd>("dest04", eD));
    mpHerds.emplace("dest05", std::make_shared<MockHerd>("dest05", eD));
    mpHerds.emplace("dest06", std::make_shared<MockHerd>("dest06", eD));
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("outside",  eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("outside",  eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["outside"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    mvOutPendingMoves["outside"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    MovesMatrix expMovesMatrix1 = MovesMatrix();
    expMovesMatrix1[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix1[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix1[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    expMovesMatrix1[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    expMovesMatrix1[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    expMovesMatrix1[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7]));
    expMovesMatrix1[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    MovesMatrix expMovesMatrix2 = MovesMatrix();
    expMovesMatrix2[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    MovesMatrix expMovesMatrix3 = MovesMatrix();
    expMovesMatrix3[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    expMovesMatrix3[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest06", vpAnimal[12]));
    expMovesMatrix3[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    expMovesMatrix3[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[14]));
    expMovesMatrix3[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[15]));
    expMovesMatrix3[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[16]));
    expMovesMatrix3[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    expMovesMatrix3[eD][eA].push_back(std::make_shared<MovementForMatrix>("source05", "dest00", vpAnimal[20]));
    expMovesMatrix3[eD][eA].push_back(std::make_shared<MovementForMatrix>("source06", "dest00", vpAnimal[21]));
    expMovesMatrix3[eD][eA].push_back(std::make_shared<MovementForMatrix>("source07", "dest00", vpAnimal[22]));
    expMovesMatrix3[eD][eA].push_back(std::make_shared<MovementForMatrix>("source08", "dest00", vpAnimal[23]));
    expMovesMatrix3[eD][eC].push_back(std::make_shared<MovementForMatrix>("source09", "dest02", vpAnimal[24]));
    MovesMatrix expMovesMatrix4 = MovesMatrix();
    expMovesMatrix4[eD][eC].push_back(std::make_shared<MovementForMatrix>("source10", "dest02", vpAnimal[25]));
    expMovesMatrix4[eD][eC].push_back(std::make_shared<MovementForMatrix>("source11", "dest02", vpAnimal[26]));
    expMovesMatrix4[eD][eC].push_back(std::make_shared<MovementForMatrix>("source12", "dest02", vpAnimal[27]));
    expMovesMatrix4[eD][eC].push_back(std::make_shared<MovementForMatrix>("source13", "dest02", vpAnimal[28]));

    MovesRewiring iMovesRewiring = MovesRewiring();
    iMovesRewiring.fillMatrices(mpHerds, vIds, mvOutPendingMoves);

    CHECK(iMovesRewiring.getMatricesVector()[2].isEmpty());
    CHECK(iMovesRewiring.getMatricesVector()[4].isEmpty());
    for (auto i = 6; i < iMovesRewiring.getMatricesVector().size(); ++i){
        CHECK(iMovesRewiring.getMatricesVector()[i].isEmpty());
    }
    CHECK(expMovesMatrix1 == iMovesRewiring.getMatricesVector()[1]);
    CHECK(expMovesMatrix2 == iMovesRewiring.getMatricesVector()[0]);
    CHECK(expMovesMatrix3 == iMovesRewiring.getMatricesVector()[5]);
    CHECK(expMovesMatrix4 == iMovesRewiring.getMatricesVector()[3]);
}

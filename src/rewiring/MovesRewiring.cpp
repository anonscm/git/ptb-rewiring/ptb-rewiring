
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "MovesRewiring.h"

MovesRewiring::MovesRewiring(){
}

std::vector<MovesMatrix>& MovesRewiring::getMatricesVector(){
    return vMovesMatrices;
}

void MovesRewiring::fillMatrices(std::map<std::string, std::shared_ptr<Herd>>& mpHerds, std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves){
    Parameters& iParams = Parameters::getInstance();
    vMovesMatrices.assign(iParams.vAgeThresForMoves.size() + iParams._nbStagesOfLactation, MovesMatrix()); //need to be done here for tests (not in rewire() function)
    std::string destinationId;
    int sourceStatus;
    int destStatus;
    for (auto& itDestToMoves : mvOutPendingMoves){
        destinationId = itDestToMoves.first;
        if (destinationId == iParams.outsideMetapop) continue;
        if (iParams.useSelectedHerdsToRewire and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), destinationId) == vHerdIdsToRewire.end())) continue;
        for (auto& itMoves : itDestToMoves.second){
            if (iParams.useSelectedHerdsToRewire and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), itMoves.sourceId) == vHerdIdsToRewire.end())) continue;
            if (iParams.useBreed and itMoves.breed != iParams.breedToRewire) continue;
            if (itMoves.sourceId == iParams.outsideMetapop) continue;
            sourceStatus = itMoves.sourceStatus;
            destStatus = mpHerds.at(destinationId)->getStatus();
            if (sourceStatus != destStatus){
                auto pMove = std::make_shared<MovementForMatrix>(itMoves.sourceId, destinationId, itMoves.pAnimal);
                auto age = itMoves.pAnimal->getAge();
                if (age < iParams._ageFirstCalving){
                    for (auto i = 0; i < iParams.vAgeThresForMoves.size(); ++i){
                        if (age < iParams.vAgeThresForMoves[i]){
                            vMovesMatrices[i][sourceStatus][destStatus].push_back(pMove);
                            break;
                        }
                    }
                } else {
                    auto parity = itMoves.pAnimal->getParity();
                    parity = (parity > iParams._nbStagesOfLactation) ? iParams._nbStagesOfLactation : parity;
                    if (parity != 0){
                        vMovesMatrices[iParams.vAgeThresForMoves.size() + parity - 1][sourceStatus][destStatus].push_back(pMove);
                    } else {
                        std::cout << "old heifer..." << std::endl;
                        vMovesMatrices[iParams.vAgeThresForMoves.size() - 1][sourceStatus][destStatus].push_back(pMove);
                    }
                }
            }
        }
    }
}

void MovesRewiring::rewire(std::map<std::string, std::shared_ptr<Herd>>& mpHerds, std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside){
    fillMatrices(mpHerds, vHerdIdsToRewire, mvOutPendingMoves);
    for (auto& iMovesMatrix : vMovesMatrices){
        iMovesMatrix.rewire(mvOutPendingMoves);
        if (Parameters::getInstance().doRewiringForbiddenMoves){
            iMovesMatrix.handleMovesImpossibleToRewire(mvOutPendingMoves, mvPendingMovesFromOutside);
        }
    }
}

void MovesRewiring::rewireBuyers(std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves){
    Parameters& iParams = Parameters::getInstance();
    std::string destinationId;
    for (auto& itDestToMoves : mvOutPendingMoves){
        destinationId = itDestToMoves.first;
        if (destinationId == iParams.outsideMetapop) continue;
        if (iParams.useSelectedHerdsToRewire and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), destinationId) == vHerdIdsToRewire.end())) continue;
        for (auto& itMoves : itDestToMoves.second){
            if (iParams.useBreed and itMoves.breed != iParams.breedToRewire) continue;
            if (itMoves.sourceId == iParams.outsideMetapop) continue;
            itMoves.pAnimal->setHealthStatus(enumAnimal::S);
            itMoves.rewiringType = eHealthyAnimal;
        }
    }
}

void MovesRewiring::rewireSellers(std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves){
    Parameters& iParams = Parameters::getInstance();
    std::string destinationId;
    for (auto& itDestToMoves : mvOutPendingMoves){
        destinationId = itDestToMoves.first;
        if (destinationId == iParams.outsideMetapop) continue;
        for (auto& itMoves : itDestToMoves.second){
            if (iParams.useSelectedHerdsToRewire and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), itMoves.sourceId) == vHerdIdsToRewire.end())) continue;
            if (iParams.useBreed and itMoves.breed != iParams.breedToRewire) continue;
            if (itMoves.sourceId == iParams.outsideMetapop) continue;
            itMoves.pAnimal->setHealthStatus(enumAnimal::S);
            itMoves.rewiringType = eHealthyAnimal;
        }
    }
}
